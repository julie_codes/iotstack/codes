const tf = require('@tensorflow/tfjs-node');
const fs = require('fs');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const multer = require('multer');
const path = require('path');

const cors = require('cors');
const sharp = require('sharp');
var host = process.env.IP || 'localhost';
const express = require('express')
const app = express()
const mongoose = require('mongoose')
var mqtt = require('mqtt')
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/"; //mongodb+srv://juliesundar:HELENKELLER1!@cluster0.pnrsquj.mongodb.net/farmbuddies
//var url = "mongodb+srv://juliesundar:HELENKELLER1!@cluster0.pnrsquj.mongodb.net/farmbuddies";
var staticSite = __dirname + '/public';

// Load the detection class indeces
const labels = require("./model/model5custom/inverted_class_indices.json"); //model2working

//Gpio topic 
const statusTopic = 'farmwise/node01/gpio/status';
// Create an array to store video frames
const videoFrames = [];

// Enable cors
app.use(cors());

  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });	

const WebSocket = require('ws');
const http = require('http');


 // Create an HTTP server
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('WebSocket Server\n');
});

// Combine the WebSocket and HTTP server
const wss = new WebSocket.Server({ server });


// Initialize an array to keep track of connected WebSocket clients
const clients = new Map(); //Map cliemt ID's to WebSocket connections
//const clients = [];
// ...

// WebSocket server event handlers
wss.on('connection', (ws) => {
  console.log('Client connected');
  let clientId;
  
  // Add the connected WebSocket client to the array
 // clients.push(ws);

  // Handle messages from clients
  ws.on('message', (message) => {
    // Parse the received message as JSON
    try {
      const parsedMessage = JSON.parse(message);
      //console.log(parsedMessage);
      //const cameraId = parsedMessage.camera_id;
      //const frameData = parsedMessage.frame_data;
      // Process the camera ID and frame data here
      //console.log('Received camera ID:', cameraId);
      //console.log('Received frame data:', frameData);
      //capture registration
      if (parsedMessage.type === 'register'){
          clientId = parsedMessage.id;
          const clientName = parsedMessage.name;
          // store the WS connection with client info
          clients.set(clientId, {ws, name: clientName});
          console.log('Client ', clientName,' registered with ID ',clientId);
          //console.log(clients);
      }
      // Send the parsed message (camera ID and frame data) to all connected clients
      clients.forEach((client) => {
        if (client.ws.readyState === WebSocket.OPEN) {
          client.ws.send(JSON.stringify(parsedMessage));
          //console.log(message);
        }
      });
    } catch  {
      //console.error('Bin received:');
      //console.log(message);
      //Assume if not Json its binary
      clients.forEach((client) => {
        // Check if the client is in an open state
        if (client.ws.readyState === WebSocket.OPEN) {
          // Send the binary data to the client
          client.ws.send(message, { binary: true });
         // console.log("bin sent");
        }
      });
      
    }
  });

  // Handle disconnection
  ws.on('close', () => {
    // remove client from list
    if (clientId){
        clients.delete(clientId);
        console.log('Client ${clientId} disconnected')
    }
  });
});

// ...




// Route to handle image prediction
app.post('/detect', upload.single('image'), async (req, res) => {
  try {
    // Load the model
    let modelPath = 'model/model5custom/model.json'; //note model2 working
    let model = await tf.loadLayersModel(`file://${modelPath}`);
    console.log('Model loaded');

    // Load image from file system
    const imagePath = req.file.path;
    let img = fs.readFileSync(imagePath);

    // Decode and preprocess image
    let imageTensor = tf.node.decodeImage(img);
    let offset = tf.scalar(255)
    let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
    let tensorImg_scaled = tensorImg.div(offset);
    
    let prediction = await model.predict(tensorImg_scaled).data();
    let predicted_class = tf.argMax(prediction);
    let class_idxs = Array.from(predicted_class.dataSync());
    let diseases = [];

    for (let class_idx of class_idxs) {
      let predictedDisease = labels[class_idx];
      let [name, disease] = predictedDisease.split('___');
      name = name.replaceAll('_', ' ');
      disease = disease.replaceAll('_', ' ');
      diseases.push({
        name,
        disease,
      });
    }

    const base64Image = Buffer.from(img).toString('base64');
    const response = {
      img:base64Image,
      diseases:diseases
    };
    console.log(response);
    res.send(response); 
  } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while predicting image.');
  }
});

const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database'))

app.use(express.json())

var client  = mqtt.connect([{host:'localhost',port:'1883'}]) //var client  = mqtt.connect([{host:'broker.hivemq.com',port:'1883'}]) //
 console.log("Nodejs Server Started!");
 


client.on('connect', function () {
   client.subscribe([
    'farmwise/node01/sensors/data',
    'farmwise/node01/gpio/status',
    'farmwise/node01/gpio/control',
    'farmwise/node01/stream'
  ], function (err) {
	  console.log("farmwise topics");
      if(err)
      console.log(err)
  })
})



app.get('/control-gpio', function (req, res) {
  // Extract GPIO pin and command from the query parameters
  const { pin, command } = req.query;

  // Publish GPIO control command to the MQTT topic
  client.publish('farmwise/node01/gpio/control', JSON.stringify({ pin, command }), function (err) {
    if (err) {
      console.error('Failed to publish GPIO control command:', err);
      res.status(500).send('Failed to control GPIO');
    } else {
      console.log('GPIO control command published:', pin, command);
      res.status(200).send('GPIO control command sent');
    }
  });
});



 
// Handle MQTT messages
client.on('message', function (topic, message) {
  console.log('Received MQTT message on topic:', topic);
  console.log('Message:', message.toString());
  // Rest of your message handling code...
  console.log('Received video frame on topic:', topic);
  console.log('Message length:', message.length);
  if (topic === statusTopic) {
    console.log('Received status update:', message.toString());
  } else if (topic === 'farmwise/node01/stream') {
    // Broadcast video frames to WebSocket clients
    videoFrames.push(message);
    console.log(message);
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
        console.log(message);
      }
    });
  } else {
    // Handle other MQTT topics as needed
  }
}); 


//check if data json or not
function json_check(data) {
    try {
       // JSON.parse(data);
	 msg = JSON.parse(data.toString()); // t is JSON so handle it how u want
	 // add a timestamp to the JSON object
     msg.timestamp = new Date();
	 msg.gateway = "FarmBuddy_Gateway01";
	 
    } catch (e) {
		console.log("message could not valid json " + data.toString);
        return false;
    }
	 console.log(msg);
	 var msgobj = { "msg": msg }; // message object
    Mongo_insert(msgobj)
	console.log(msgobj);
}

//insert data in mongodb
function Mongo_insert(msg){
MongoClient.connect(url, function(err, db ) {
    if (err) throw err;
    var dbo = db.db("farmbuddies");
    dbo.collection("node02").insertOne(msg, function(err, res) {
      if (err) throw err;
	   console.log("data stored");
      //db.close();
    });

    //Fetech from nested json obj in collection
    dbo.collection('node02').find({}).toArray(function(err, result) {
      if (err) throw err;
      // send data as response to client
     // app.get('/data', function(req, res) {
     //   res.send(result);
     // });
     app.use('/data', subscribersRouter)
    // res.send(result);
      db.close();
    });

  }); 
}

const subscribersRouter = require('./routes/subscribers')
app.use('/data', subscribersRouter)

// ENABLE CORS for Express (so swagger.io and external sites can use it remotely .. SECURE IT LATER!!)
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Routes here

app.use('/', express.static(staticSite));


// Serve the HTML file with route
app.get('/', (req, res) => {
res.sendFile(__dirname + '/index.html');
});
	

// Start the HTTP server on port 3000
const PORT = 4000;
server.listen(PORT, () => {
  console.log(`Server listenisng on port ${PORT}`);
});

app.listen(3000, () => console.log('Server Started port 3000'))