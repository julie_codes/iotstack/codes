const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const app = express();

// Serve the HTML page
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

const httpServer = http.createServer(app);
const cameraServer = http.createServer();

const cameraWss = new WebSocket.Server({ noServer: true });
const htmlWss = new WebSocket.Server({ noServer: true });

// Map camera IDs to WebSocket clients
const clientsCamera1 = new Set();
const clientsCamera2 = new Set();
const clientsHTML = new Set();

// Handle WebSocket connections for cameras
cameraWss.on('connection', (ws, request) => {
  const pathname = new URL(request.url, 'http://localhost').pathname;

  if (pathname === '/camera/1') {
    console.log('Client connected to Camera 1');
    clientsCamera1.add(ws);

    ws.on('close', () => {
      console.log('Client disconnected from Camera 1');
      clientsCamera1.delete(ws);
    });
  } else if (pathname === '/camera/2') {
    console.log('Client connected to Camera 2');
    clientsCamera2.add(ws);

    ws.on('close', () => {
      console.log('Client disconnected from Camera 2');
      clientsCamera2.delete(ws);
    });
  } else {
    console.log('Invalid WebSocket path:', pathname);
    ws.close();
  }
});

// Handle WebSocket connections for HTML client
htmlWss.on('connection', (ws) => {
  console.log('HTML client connected');
  clientsHTML.add(ws);

  ws.on('close', () => {
    console.log('HTML client disconnected');
    clientsHTML.delete(ws);
  });

  // Handle messages from HTML client
  ws.on('message', (message) => {
    // Handle incoming WebSocket messages from HTML client here
    // You can broadcast messages to other connected clients as needed
  });
});

// Start the servers
const httpPort = 3000;
const cameraPort = 6000;

httpServer.listen(httpPort, () => {
  console.log(`HTTP server listening on port ${httpPort}`);
});

cameraServer.listen(cameraPort, () => {
  console.log(`Camera WebSocket server listening on port ${cameraPort}`);
});

// Attach WebSocket servers to the HTTP servers
cameraServer.on('upgrade', (request, socket, head) => {
  cameraWss.handleUpgrade(request, socket, head, (ws) => {
    cameraWss.emit('connection', ws, request);
  });
});

httpServer.on('upgrade', (request, socket, head) => {
  htmlWss.handleUpgrade(request, socket, head, (ws) => {
    htmlWss.emit('connection', ws, request);
  });
});
