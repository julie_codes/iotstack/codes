
  // Get all the settings icons
  const settingsIcons = document.querySelectorAll('.settings-icon');

  // Attach the event listener to each settings icon
  settingsIcons.forEach(icon => {
    icon.addEventListener('click', toggleSettingsMenu);
  });

  function toggleSettingsMenu(event) {
    // Prevent the click event from bubbling up to the parent div
    event.stopPropagation();
    console.log('Toggle settings menu called');

    // Check if the settings menu is already open
    const settingsMenu = document.querySelector('.settings-menu');
    if (settingsMenu) {
      // If menu is open, close it
      closeSettingsMenu();
    } else {
      // If menu is closed, open it

      // Create the settings menu dynamically
      const settingsMenu = document.createElement('div');
      settingsMenu.classList.add('settings-menu');

      // Add menu options
      const deleteOption = document.createElement('div');
      deleteOption.textContent = 'Delete';
      deleteOption.addEventListener('click', deleteDiv);
      settingsMenu.appendChild(deleteOption);

      const addEndpointOption = document.createElement('div');
      addEndpointOption.textContent = 'Add Endpoint';
      addEndpointOption.addEventListener('click', addEndpoint);
      settingsMenu.appendChild(addEndpointOption);

      // Position the menu relative to the clicked settings icon
      const icon = event.currentTarget;
      const rect = icon.getBoundingClientRect();
      settingsMenu.style.left = rect.left + 'px';
      settingsMenu.style.top = rect.bottom + 'px';

      // Add the settings menu to the parent container of the settings icon
      const parentContainer = icon.parentElement;
      parentContainer.appendChild(settingsMenu);

      // Close the menu when clicking outside of it
      document.addEventListener('click', closeSettingsMenu);
    }
  }

  function deleteDiv() {
    const div = this.parentNode.parentNode;
    div.parentNode.removeChild(div);
  }

function addEndpoint() {
  // Prompt the user to enter the endpoint
  const endpoint = prompt('Enter the endpoint:');

  if (endpoint) {
    // Create a new paragraph element to display the endpoint
    const endpointElement = document.createElement('p');
    endpointElement.textContent = `Endpoint: ${endpoint}`;

    // Get the parent task div
    const taskDiv = this.closest('.task');

    // Append the endpoint element to the task div
    taskDiv.appendChild(endpointElement);
  }

  // Close the settings menu
  closeSettingsMenu();
}


  function closeSettingsMenu() {
    const settingsMenu = document.querySelector('.settings-menu');
    if (settingsMenu) {
      settingsMenu.parentNode.removeChild(settingsMenu);
    }
    // Remove the click event listener for closing the menu
    document.removeEventListener('click', closeSettingsMenu);
  }
