const tf = require('@tensorflow/tfjs-node');
const MODEL_PATH = 'model3/model.json';
const IMAGE_SIZE = 224;

const loadModel = async () => {
  const model = await tf.loadLayersModel(`file://${MODEL_PATH}`);
  return model;
};

const predict = async (model, imagePath) => {
  const imageBuffer = fs.readFileSync(imagePath);
  const imageTensor = tf.node.decodeImage(imageBuffer);
  const processedImage = tf.image.resizeBilinear(imageTensor, [IMAGE_SIZE, IMAGE_SIZE]).div(255.0);
  const prediction = model.predict(processedImage.reshape([-1, IMAGE_SIZE, IMAGE_SIZE, 3]));
  const scores = prediction.arraySync()[0];
  return scores;
};

(async () => {
  const model = await loadModel();
  const imagePath = 'potato.jpg'; // Replace with your own potato image
  const scores = await predict(model, imagePath);
  console.log(scores);
})();
