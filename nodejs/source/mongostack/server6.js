
//Server Variables
var host = process.env.IP || 'localhost';
var port = process.env.PORT || 8080;

var staticSite = __dirname + '/public';
var swaggerUI = __dirname + '/public/swag-ui';

var express = require('express');
var app = express();


var mqtt = require('mqtt')
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";  // "mongodb://localhost:27017/iotstack";
var client  = mqtt.connect([{host:'localhost',port:'1883'}]) //var client  = mqtt.connect([{host:'broker.hivemq.com',port:'1883'}]) //
 console.log("Nodejs Server Started!");
 
// on mqtt conect subscribe on tobic test 
client.on('connect', function () {
  client.subscribe('juliesundar/codettes/01', function (err) {
	 console.log("sub scribing to test topic");
      if(err)
      console.log(err)
  })
})

 //when recive message 
client.on('message', function (topic, message) {
  json_check(message)
})

//check if data json or not
function json_check(data) {
    try {
       // JSON.parse(data);
	 msg = JSON.parse(data.toString()); // t is JSON so handle it how u want
    } catch (e) {
		console.log("message could not valid json " + data.toString);
        return false;
    }
	 console.log(msg);
	 var msgobj = { "msg": msg }; // message object
    Mongo_insert(msgobj)
	console.log(msgobj);
}

//insert data in mongodb
function Mongo_insert(msg){
MongoClient.connect(url, function(err, db ) {
    if (err) throw err;
    var dbo = db.db("iotstack");
    dbo.collection("esps").insertOne(msg, function(err, res) {
      if (err) throw err;
	   console.log("data stored");
      //db.close();
    });
  }); 
}


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router


// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.use('/', express.static(swaggerUI));

// ENABLE CORS for Express (so swagger.io and external sites can use it remotely .. SECURE IT LATER!!)
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/', express.static(staticSite));
// Use router for all /api requests
app.use('/api', router );

if (! process.env.C9_PID) {
    console.log('Running at http://'+ host +':' + port);
}
app.listen(port, function() { console.log('Listening')});
