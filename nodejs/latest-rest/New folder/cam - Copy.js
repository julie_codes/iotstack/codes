const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const app = express();
const server = http.createServer(app);

// Serve the HTML page (same as before)
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Create WebSocket servers for each camera stream
const camera1Wss = new WebSocket.Server({ noServer: true });
const camera2Wss = new WebSocket.Server({ noServer: true });

// Map camera IDs to WebSocket clients
const clientsCamera1 = new Map();
const clientsCamera2 = new Map();
const camClients= new Map();
var clients=[];

// Handle WebSocket connections for Camera 1
camera1Wss.on('connection', (ws) => {
  console.log('Client connected to Camera 1');
  clientsCamera1.set(ws, true);

// Handle messages from clients
  ws.on('message', (message) => {
    // Parse the received message as JSON
    try {
      const parsedMessage = JSON.parse(message);
      //console.log(parsedMessage);
      //capture stream registration
      if (parsedMessage.type === 'register'){
          clientId = parsedMessage.id;
          const clientName = parsedMessage.name;
          // store the WS connection with camClient info
          // todo: add camera settings/IP etc to opts:{framerate, size, URL, Port, etc etc}
          camClients.set(clientId, {ws, name: clientName});
          console.log('Client ', clientName,' registered with ID ',clientId);
          //console.log(clients);
      }
      // Send any other JSON parsed message (camera ID and frame data) to all connected clients
      clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify(parsedMessage));
          console.log(message);
        }
      });
    } catch  {
      // ### Todo: check contents sanity (before forwarding)
      // Todo: send only to subscribed_to_feeds users
      // Assume if not Json its binary or just text
      clients.forEach((client) => {
        // Check if the client is in an open state
        if (client.readyState === WebSocket.OPEN) {
          // Send the binary data to the client
          client.send(message, { binary: true });
          console.log("bin sent");
        };
	});
  ws.on('close', () => {
    console.log('Client disconnected from Camera 1');
    clientsCamera1.delete(ws);
  });
});

// Handle WebSocket connections for Camera 2
camera2Wss.on('connection', (ws) => {
  console.log('Client connected to Camera 2');
  clientsCamera2.set(ws, true);

  ws.on('close', () => {
    console.log('Client disconnected from Camera 2');
    clientsCamera2.delete(ws);
  });
});

// Upgrade HTTP requests to WebSocket based on the path
server.on('upgrade', (request, socket, head) => {
  const pathname = new URL(request.url, 'http://localhost').pathname;

  if (pathname === '/camera/1') {
    camera1Wss.handleUpgrade(request, socket, head, (ws) => {
      camera1Wss.emit('connection', ws, request);
    });
  } else if (pathname === '/camera/2') {
    camera2Wss.handleUpgrade(request, socket, head, (ws) => {
      camera2Wss.emit('connection', ws, request);
    });
  } else {
    socket.destroy();
  }
});

// Start the server
const PORT = 6000;
server.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
