const express = require('express');
const multer = require('multer');
const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const fs = require('fs');
const path = require('path');
const cors = require('cors');

// Set up the app and middleware
const app = express();
const upload = multer({ dest: 'uploads/' });

// Load the Inception model
//const modelPath = 'https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/feature_vector/4';
const modelPath = 'https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1';
let model;
(async () => {
  model = await tf.loadGraphModel(modelPath, { fromTFHub: true });
  console.log('Inception model loaded');
})();

// Load the COCO-SSD model
let cocoModel;
(async () => {
  cocoModel = await cocoSsd.load();
  console.log('COCO-SSD model loaded');
})();

  // Create an Express app
 // const app = express();

  app.use(cors());
  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

// Define a route to classify an image using Inception and detect pests using COCO-SSD
app.post('/classify', upload.single('image'), async (req, res) => {
  try {
    // Load the image from disk
    const imageBuffer = fs.readFileSync(req.file.path);
    const image = tf.node.decodeImage(imageBuffer);

    // Classify the image using Inception
    const inceptionOutput = model.predict(image);
    const inceptionPredictions = inceptionOutput.arraySync()[0];
    const topPrediction = inceptionPredictions.indexOf(Math.max(...inceptionPredictions));
    const vegetables = ['carrot', 'corn', 'lettuce', 'pepper', 'tomato'];
    const predictedVegetable = vegetables[topPrediction];

    // Detect pests using COCO-SSD
    const cocoPredictions = await cocoModel.detect(image);
    const pestsPresent = cocoPredictions.some((prediction) => prediction.class === 'person');

    // Construct the response object
    const response = {
      vegetable: predictedVegetable,
      pestsPresent: pestsPresent,
    };

    // Send the response
    res.json(response);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Start the server
const port = 3000;
app.listen(port, () => console.log(`Server started on port ${port}`));
