import tensorflow as tf
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.mobilenet_v2 import MobileNetV2
from keras.models import Sequential
from keras.layers import GlobalAveragePooling2D, Dense, Dropout

# 1. Prepare your dataset
datagen = ImageDataGenerator(
    rescale=1./255,
    validation_split=0.2,
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

train_generator = datagen.flow_from_directory(
    'training/PlantVillage',
    target_size=(224, 224),
    batch_size=32,
    class_mode='categorical',
    subset='training')

validation_generator = datagen.flow_from_directory(
    'training/PlantVillage',
    target_size=(224, 224),
    batch_size=32,
    class_mode='categorical',
    subset='validation')

# 2. Define your model
base_model = MobileNetV2(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

model = Sequential()
model.add(base_model)
model.add(GlobalAveragePooling2D())
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(3, activation='softmax'))

# Freeze the pre-trained weights
for layer in base_model.layers:
    layer.trainable = False

# 3. Compile your model
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])

# 4. Fine-tune the model
# Train the entire model with a small learning rate for a few epochs
model.fit(train_generator,
          epochs=10,
          validation_data=validation_generator)

# Unfreeze some of the top layers of the model and continue training with a larger learning rate
for layer in base_model.layers[-20:]:
    layer.trainable = True

model.compile(optimizer=tf.keras.optimizers.Adam(lr=0.0001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])

model.fit(train_generator,
          epochs=20,
          validation_data=validation_generator)

# 5. Save your model in Keras HDF5 format
model.save('custom/custom_model.h5')

# 6. Convert your Keras model to TensorFlow.js format
tf.converters.save_keras_model(model, 'custom/json/tfjs_model')

import tensorflowjs as tfjs

# Load the Keras HDF5 model
model = tf.keras.models.load_model('custom/custom_model.h5')

# Convert the Keras model to TensorFlow.js format with model.json and weights.bin files
tfjs.converters.save_keras_model(model, 'custom/tfjs_model')

