https://docs.google.com/document/d/1mZKVFgMkpK4Pyb88_acRsYAlFUB60dYCWDteSSK0Io8/edit?usp=sharing

Starting all processes.

https://drive.google.com/drive/folders/12-wXZRn0rLKpSN_1kz3k48RiTErkulju?usp=sharing

Upload firmware code on esp32 make sure to connect to network ssid pass and mqtt  broker is set to local ip of pc. Note: in GetWeatherData Dont add float to variables add it global and remove here.

Cmd to Program Files / mosquitto run command Mosquitto -v -c test.conf

Start nodejs server.js mongostack 

Start mongo db on local computer

Note versions of node and mongo need to be this or it will not work

Nvm install 16.14.2

Nvm use 16.14.2

Mongodb version on windows 5.0.9

https://www.npackd.org/p/org.mongodb.MongoDB64/5.0.9

BME SPIFF ESP32 MQTT CHART 

Todo make charts for the rest altitude heatindex etc!

https://drive.google.com/file/d/1rOoq601CHhmyImAcNNN6vZymSpe_XjJF/view?usp=sharing

Todo https://medium.com/@ladolcefarniente/making-weather-real-time-charts-using-esp-32-and-bme280-ac1b680f35a6

CHART http://localhost:5000/indexChart.html

Timestamp

https://steveridout.com/mongo-object-time/

https://randomnerdtutorials.com/esp32-plot-readings-charts-multiple/

Day 5:

TODO;

https://icircuit.net/arduino-esp32-home-automation-simple-off-control-using-mqtt/2272

MQTT OVER WEBSOCKET

Enable local mosquitto websocket port

port 1883

listener 9001

protocol websockets

socket_domain ipv4

allow_anonymous true

https://jpmens.net/2014/07/03/the-mosquitto-mqtt-broker-gets-websockets-support/

https://github.com/jpmens/simple-mqtt-websocket-example

NODE JS SERVER CODE

//Server Variables

var host = process.env.IP || 'localhost';

var port = process.env.PORT || 8080;

var staticSite = __dirname + '/public';

var swaggerUI = __dirname + '/public/swag-ui';

var express = require('express');

var app = express();

var mqtt = require('mqtt')

var MongoClient = require('mongodb').MongoClient;

var url = "mongodb://localhost:27017/";  // "mongodb://localhost:27017/iotstack";

var client  = mqtt.connect([{host:'localhost',port:'1883'}]) //var client  = mqtt.connect([{host:'broker.hivemq.com',port:'1883'}]) //

 console.log("Nodejs Server Started!");

// on mqtt conect subscribe on tobic test 

client.on('connect', function () {

  client.subscribe('juliesundar/codettes/01', function (err) {

console.log("sub scribing to test topic");

      if(err)

      console.log(err)

  })

})

 //when recive message 

client.on('message', function (topic, message) {

  json_check(message)

})

//check if data json or not

function json_check(data) {

    try {

       // JSON.parse(data);

msg = JSON.parse(data.toString()); // t is JSON so handle it how u want

    } catch (e) {

console.log("message could not valid json " + data.toString);

        return false;

    }

console.log(msg);

var msgobj = { "msg": msg }; // message object

    Mongo_insert(msgobj)

console.log(msgobj);

}

//insert data in mongodb

function Mongo_insert(msg){

MongoClient.connect(url, function(err, db ) {

    if (err) throw err;

    var dbo = db.db("iotstack");

    dbo.collection("esp").insertOne(msg, function(err, res) {

      if (err) throw err;

   console.log("data stored");

      //db.close();

    });

  }); 

}

// ROUTES FOR OUR API

// =============================================================================

var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)

router.use('/', express.static(swaggerUI));

// ENABLE CORS for Express (so swagger.io and external sites can use it remotely .. SECURE IT LATER!!)

app.use(function(req, res, next) {

  res.header("Access-Control-Allow-Origin", "*");

  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  next();

});

app.use('/', express.static(staticSite));

// Use router for all /api requests

app.use('/api', router );

if (! process.env.C9_PID) {

    console.log('Running at http://'+ host +':' + port);

}

app.listen(port, function() { console.log('Listening')});

ESP32 CODE BME

#include<PubSubClient.h>

#include <ArduinoJson.h>

#include <EnvironmentCalculations.h>

#include <BME280I2C.h>

#include <WiFi.h>

#include <WebServer.h>

#include <Wire.h>

const char * mqtt_broker = "192.168.1.2";//"broker.hivemq.com"; // //"broker.hivemq.com"; //"192.168.1.2"; //"broker.hivemq.com";// "192.168.1.2"; // // CHANGE THIS ACCORDINGLY

const char * mqtt_topic = "juliesundar/codettes/01"; // CHANGE SensorID here!

const char * mqtt_User = "";

const char * mqtt_Password = "";

// Assumed environmental values:

float referencePressure = 1018.6; // hPa local QFF (official meteor-station reading)

float outdoorTemp = 4.7; // °C  measured local outdoor temp.

float barometerAltitude = 1650.3; // meters ... map readings + barometer position

BME280I2C::Settings settings(

  BME280::OSR_X1,

  BME280::OSR_X1,

  BME280::OSR_X1,

  BME280::Mode_Forced,

  BME280::StandbyTime_1000ms,

  BME280::Filter_16,

  BME280::SpiEnable_False,

  BME280I2C::I2CAddr_0x76

);

WiFiClient espClient;

PubSubClient client(espClient);

BME280I2C bme(settings);

/*

   getLocalTimeNTP: Get Epoch Time Stamp from NTP server

*/

unsigned long getLocalTimeNTP() {

  time_t now;

  struct tm timeinfo;

  if (!getLocalTime( & timeinfo)) {

    Serial.println("Failed to obtain time");

    return 0;

  }

  time( & now);

  return now;

}

float temp, hum, pres, altitude;

char output[1023];

/*Put your SSID & Password*/

const char * ssid = "Virus"; //"HUAWEI-3FDA";// // Enter SSID here

const char * password = "RedEyeJedi44"; //"YB7DJY63ARE"; // //Enter Password here

WebServer server(80);

void setup() {

  Serial.begin(115200);

//connect to your local wi-fi network

  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network

  while (WiFi.status() != WL_CONNECTED) {

    delay(1000);

    Serial.print(".");

  }

  Serial.println("");

  Serial.println("WiFi connected..!");

  Serial.print("Got IP: ");

  Serial.println(WiFi.localIP());

  client.setServer(mqtt_broker, 1883);

  if(initMqtt()){

    client.publish("juliesundar/codettes/01", "Hello from ESP32");

    }

  server.on("/", handle_OnConnect);

  server.onNotFound(handle_NotFound);

  server.begin();

  Serial.println("HTTP server started");

  Wire.begin();

  initBMP();

  Serial.println("Connecting to ");

  Serial.println(ssid);

}

unsigned long lastMqttTime = millis();

const unsigned long Mqtt_INTERVAL_MS = 2000;

void loop() {

  server.handleClient();

  // Handle MQTT Client

  client.loop();

  // Send regular mqtt data

  if ((millis() - lastMqttTime) > Mqtt_INTERVAL_MS) {

    if(client.connected())

    {

       GetWeatherData();

       handle_MqttData();

       lastMqttTime = millis();

    } else

    {

      initMqtt();

    }

  }

}

bool initMqtt()

{

  if (!client.connected()) {

    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP32Client", mqtt_User, mqtt_Password )) {

      Serial.println("connected");

      return true;

    } else {

      Serial.print("failed with state ");

      Serial.print(client.state());

      delay(2000);

      return false;

    }

  } 

}

bool initBMP()

{

  while (!bme.begin()) {

    Serial.println("Could not find BME280 sensor!");

    delay(1000);

  }

  switch (bme.chipModel()) {

    case BME280::ChipModel_BME280:

      Serial.println("Found BME280 sensor! Success.");

      break;

    case BME280::ChipModel_BMP280:

      Serial.println("Found BMP280 sensor! No Humidity available.");

      break;

    default:

      Serial.println("Found UNKNOWN sensor! Error!");

  }

  Serial.print("Assumed outdoor temperature: ");

  Serial.print(outdoorTemp);

  Serial.print("°C\nAssumed reduced sea level Pressure: ");

  Serial.print(referencePressure);

  Serial.print("hPa\nAssumed barometer altitude: ");

  Serial.print(barometerAltitude);

  Serial.println("m\n***************************************");

}

void GetWeatherData() {

  //float temp(NAN), hum(NAN), pres(NAN);

  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);

  BME280::PresUnit presUnit(BME280::PresUnit_hPa);

  bme.read(pres, temp, hum, tempUnit, presUnit);

  EnvironmentCalculations::AltitudeUnit envAltUnit = EnvironmentCalculations::AltitudeUnit_Meters;

  EnvironmentCalculations::TempUnit envTempUnit = EnvironmentCalculations::TempUnit_Celsius;

  /// To get correct local altitude/height (QNE) the reference Pressure

  ///    should be taken from meteorologic messages (QNH or QFF)

  float altitude = EnvironmentCalculations::Altitude(pres, envAltUnit, referencePressure, outdoorTemp, envTempUnit);

  float dewPoint = EnvironmentCalculations::DewPoint(temp, hum, envTempUnit);

  /// To get correct seaLevel pressure (QNH, QFF)

  ///    the altitude value should be independent on measured pressure.

  /// It is necessary to use fixed altitude point e.g. the altitude of barometer read in a map

  float seaLevel = EnvironmentCalculations::EquivalentSeaLevelPressure(barometerAltitude, temp, pres, envAltUnit, envTempUnit);

  float absHum = EnvironmentCalculations::AbsoluteHumidity(temp, hum, envTempUnit);

}

void handle_MqttData() {

StaticJsonDocument<1023> doc;

  doc["temp"] = temp; 

  doc["humid"] = hum;

  char out[128];

  int b =serializeJson(doc, out);

  serializeJson(doc, Serial);

  Serial.print("bytes ->  ");

  Serial.print(b,DEC);

  // add if delivered or not

  client.publish((char*)mqtt_topic, out) ? Serial.println(" -> delivered") : Serial.println(" -> failed");

}

void handle_OnConnect() {

  GetWeatherData();

  server.send(200, "text/html", SendHTML(temp, hum, pres, altitude));

}

void handle_NotFound() {

  server.send(404, "text/plain", "Not found");

}

// Default HTML page

String SendHTML(float temperature, float humidity, float pressure, float altitude) {

  String ptr = "<!DOCTYPE html> <html>\n";

  ptr += "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";

  ptr += "<title>ESP32 Weather Station</title>\n";

  ptr += "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";

  ptr += "body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;}\n";

  ptr += "p {font-size: 24px;color: #444444;margin-bottom: 10px;}\n";

  ptr += "</style>\n";

  ptr += "</head>\n";

  ptr += "<body>\n";

  ptr += "<div id=\"webpage\">\n";

  ptr += "<h1>ESP32 Weather Station</h1>\n";

  ptr += "<p>Temperature: ";

  ptr += temperature;

  ptr += "&deg;C</p>";

  ptr += "<p>Humidity: ";

  ptr += humidity;

  ptr += "%</p>";

  ptr += "<p>Pressure: ";

  ptr += pressure;

  ptr += "hPa</p>";

  ptr += "<p>Altitude: ";

  ptr += altitude;

  ptr += "m</p>";

  ptr += "</div>\n";

  ptr += "</body>\n";

  ptr += "</html>\n";

  return ptr;

}

Day4 /10-8-2022

https://www.youtube.com/watch?v=DH-VSAACtBk ANSWER HERE!!!!!!!!!!!!!

Mosquitto -v -c test.conf

Mosquitto -v

Attempting MQTT connection... Failed. rc=-1 or rc=-2 or rc=-4, try again in 5 seconds

https://www.youtube.com/watch?v=CbodTTk-D18

DESCRIPTION

The GY-BME280 is a high precision combined digital pressure, humidity and temperature sensor module with I2C and SPI interfaces.

PACKAGE INCLUDES:

GY-BME280 Pressure Humidity Temperature Sensor Module

6-pin male header

KEY FEATURES OF GY-BME PRESSURE HUMIDITY TEMPERATURE SENSOR MODULE:

Temperature:  -40 to 85°C

Humidity:  0 to 100%

Pressure:  300 to 1100 hPa

Altitude:  0 to 30,000 ft

I2C and SPI Interface

3.3V operation

/*

Environment_Calculations.ino

This code shows how to record data from the BME280 environmental sensor

and perform various calculations.

GNU General Public License

Written: Dec 30 2015.

Last Updated: Oct 07 2017.

Connecting the BME280 Sensor:

Sensor              ->  Board

-----------------------------

Vin (Voltage In)    ->  3.3V

Gnd (Ground)        ->  Gnd

SDA (Serial Data)   ->  A4 on Uno/Pro-Mini, 20 on Mega2560/Due, 2 Leonardo/Pro-Micro

SCK (Serial Clock)  ->  A5 on Uno/Pro-Mini, 21 on Mega2560/Due, 3 Leonardo/Pro-Micro

 */

#include <EnvironmentCalculations.h>

#include <BME280I2C.h>

#include <Wire.h>

#define SERIAL_BAUD 115200

// Assumed environmental values:

float referencePressure = 1018.6;  // hPa local QFF (official meteor-station reading)

float outdoorTemp = 4.7;           // °C  measured local outdoor temp.

float barometerAltitude = 1650.3;  // meters ... map readings + barometer position

BME280I2C::Settings settings(

   BME280::OSR_X1,

   BME280::OSR_X1,

   BME280::OSR_X1,

   BME280::Mode_Forced,

   BME280::StandbyTime_1000ms,

   BME280::Filter_16,

   BME280::SpiEnable_False,

   BME280I2C::I2CAddr_0x76

);

BME280I2C bme(settings);

//////////////////////////////////////////////////////////////////

void setup()

{

  Serial.begin(SERIAL_BAUD);

  while(!Serial) {} // Wait

  Wire.begin();

  while(!bme.begin())

  {

    Serial.println("Could not find BME280 sensor!");

    delay(1000);

  }

  switch(bme.chipModel())

  {

     case BME280::ChipModel_BME280:

       Serial.println("Found BME280 sensor! Success.");

       break;

     case BME280::ChipModel_BMP280:

       Serial.println("Found BMP280 sensor! No Humidity available.");

       break;

     default:

       Serial.println("Found UNKNOWN sensor! Error!");

  }

  Serial.print("Assumed outdoor temperature: "); Serial.print(outdoorTemp);

  Serial.print("°C\nAssumed reduced sea level Pressure: "); Serial.print(referencePressure);

  Serial.print("hPa\nAssumed barometer altitude: "); Serial.print(barometerAltitude);

  Serial.println("m\n***************************************");

}

//////////////////////////////////////////////////////////////////

void loop()

{

   printBME280Data(&Serial);

   delay(500);

}

//////////////////////////////////////////////////////////////////

void printBME280Data

(

   Stream* client

)

{

   float temp(NAN), hum(NAN), pres(NAN);

   BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);

   BME280::PresUnit presUnit(BME280::PresUnit_hPa);

   bme.read(pres, temp, hum, tempUnit, presUnit);

   client->print("Temp: ");

   client->print(temp);

   client->print("°"+ String(tempUnit == BME280::TempUnit_Celsius ? "C" :"F"));

   client->print("\t\tHumidity: ");

   client->print(hum);

   client->print("% RH");

   client->print("\t\tPressure: ");

   client->print(pres);

   client->print(String(presUnit == BME280::PresUnit_hPa ? "hPa" : "Pa")); // expected hPa and Pa only

   EnvironmentCalculations::AltitudeUnit envAltUnit  =  EnvironmentCalculations::AltitudeUnit_Meters;

   EnvironmentCalculations::TempUnit     envTempUnit =  EnvironmentCalculations::TempUnit_Celsius;

   /// To get correct local altitude/height (QNE) the reference Pressure

   ///    should be taken from meteorologic messages (QNH or QFF)

   float altitude = EnvironmentCalculations::Altitude(pres, envAltUnit, referencePressure, outdoorTemp, envTempUnit);

   float dewPoint = EnvironmentCalculations::DewPoint(temp, hum, envTempUnit);

   /// To get correct seaLevel pressure (QNH, QFF)

   ///    the altitude value should be independent on measured pressure.

   /// It is necessary to use fixed altitude point e.g. the altitude of barometer read in a map

   float seaLevel = EnvironmentCalculations::EquivalentSeaLevelPressure(barometerAltitude, temp, pres, envAltUnit, envTempUnit);

   float absHum = EnvironmentCalculations::AbsoluteHumidity(temp, hum, envTempUnit);

   client->print("\t\tAltitude: ");

   client->print(altitude);

   client->print((envAltUnit == EnvironmentCalculations::AltitudeUnit_Meters ? "m" : "ft"));

   client->print("\t\tDew point: ");

   client->print(dewPoint);

   client->print("°"+ String(envTempUnit == EnvironmentCalculations::TempUnit_Celsius ? "C" :"F"));

   client->print("\t\tEquivalent Sea Level Pressure: ");

   client->print(seaLevel);

   client->print(String( presUnit == BME280::PresUnit_hPa ? "hPa" :"Pa")); // expected hPa and Pa only

   client->print("\t\tHeat Index: ");

   float heatIndex = EnvironmentCalculations::HeatIndex(temp, hum, envTempUnit);

   client->print(heatIndex);

   client->print("°"+ String(envTempUnit == EnvironmentCalculations::TempUnit_Celsius ? "C" :"F"));

   client->print("\t\tAbsolute Humidity: ");

   client->println(absHum);

   delay(1000);

}

Day 310/07/2022  

Installing mongodb mosquitto and node on windows test SUCCESS

Referenc architecture https://www.compose.com/articles/building-mongodb-into-your-internet-of-things-a-tutorial/

I already had nodejs installed just download and install nodejs exe

Download MongoDb

https://www.mongodb.com/try/download/community2

Windows was refusing my mosquitto connection so i check if the ports were blocked it was not i had to open  add  allow_anonymous in the mosquitto.conf file

I then started mosquitto

Navigate to path ----- mosquitto

 and tested my messages via mqtt explorer 

127.0.0.1 1883 ws

mosquitto_pub -h localhost -m "25" -t "demo/devices/arduino01"

We install mqtt explorer because mqtt lense did not connect on ws connections

http://mqtt-explorer.com/

mosquitto_pub -h localhost -m "25" -t "demo/devices/arduino01"

var mqtt = require('mqtt')

var MongoClient = require('mongodb').MongoClient;

var url = "mongodb://localhost:27017/";  // "mongodb://localhost:27017/iotstack";

var client  = mqtt.connect([{host:'localhost',port:'1883'}])

// on mqtt conect subscribe on tobic test 

client.on('connect', function () {

  client.subscribe('test', function (err) {

      if(err)

      console.log(err)

  })

})

 //when recive message 

client.on('message', function (topic, message) {

  json_check(message)

})

//check if data json or not

function json_check(data) {

    try {

       // JSON.parse(data);

msg = JSON.parse(data.toString()); // t is JSON so handle it how u want

    } catch (e) {

console.log("message could not valid json " + data.toString);

        return false;

    }

console.log(msg);

var msgobj = { "msg": msg }; // message object

    Mongo_insert(msgobj)

}

//insert data in mongodb

function Mongo_insert(msg){

MongoClient.connect(url, function(err, db ) {

    if (err) throw err;

    var dbo = db.db("iotstack");

    dbo.collection("esp").insertOne(msg, function(err, res) {

      if (err) throw err;

   console.log("data stored");

      //db.close();

    });

  }); 

}

From Mosquitto to MongoDB

For this part of the data voyage, we are going to use JavaScript and the popular Node.js platform. Node.js has a MQTT package and a MongoDB driver. On the Mac, if you don't have Node but previously installed Homebrew, just run brew install node. For other platforms, or for Homebrew-less Mac users, go to the Node.js site and click "Install" for an appropriate package. Once that's installed run npm install mqtt mongodb to install the required packages.

var mqtt=require('mqtt')  

var mongodb=require('mongodb');

var mqtt=require('mqtt')  

var mongodb=require('mongodb');  

var mongodbClient=mongodb.MongoClient;  

var mongodbURI='mongodb://username:password@server.mongohq.com:port/database'  

var deviceRoot="demo/device/"  

var collection,client;

You'll need to change that URI to match your own MongoDB instance. The code starts up by connecting to the MongoDB server and sets up a callback to the setupCollection function. This means that when it runs, once it has has completed connection, it calls back to that function...

mongodbClient.connect(mongodbURI,setupCollection);

function setupCollection(err,db) {  

  if(err) throw err;

  collection=db.collection("test_mqtt");

  client=mqtt.createClient(1883,'localhost')

  client.subscribe(deviceRoot+"+")

  client.on('message', insertEvent);

}

function insertEvent(topic,payload) {  

  var key=topic.replace(deviceRoot,'');

collection.update(  

  { _id:key },

  { $push: { events: { event: { value:payload, when:new Date() } } } },

  { upsert:true },

  function(err,docs) {

    if(err) { console.log("Insert fail"); } // Improve error handling

  }

  )

}

{

  _id: "arduino01",

  events: [

  { event: { value: "31", when: ISODate("2014-02-05T15:31:07.431Z") } },

  { event: { value: "32", when: ISODate("2014-02-05T15:31:08.432Z") } },

  { event: { value: "33", when: ISODate("2014-02-05T15:31:09.432Z") } },

  { event: { value: "32", when: ISODate("2014-02-05T15:31:10.434Z") } }...

  ]

}

Day 2 10/06/2022

INSTALLING MONGO ON RASPBERRY PI JESSIE PI 3 DID NOT WORK

Check os version on pi 

cat /etc/os-release

lsb_release -a

hostnamectl

Type the following command to find Linux kernel version:

uname -r

https://github.com/dennisdegreef/mqtt-mongo-recorder

INSTALL MONGO ON RASPBERRY PI

https://linuxhint.com/install-mongodb-raspberry-pi/

wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

sudo apt update

sudo apt-get install -y mongodb

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list sudo apt-get update

DAY1 9/25/2022

Download MoengoDb

INSTALL MONGO ON WINDOWS 10 SUCCESS

https://www.mongodb.com/try/download/community2

https://icircuit.net/let-us-learn-node-js-part-v-mongodb/2530

https://www.compose.com/articles/building-mongodb-into-your-internet-of-things-a-tutorial/

Note run cmd as administrator

Cd \Program Files\MongoDB\Server\6.0\bin

Mongod

mongod --port 27017 --dbpath "a mongodb storage actual path e.g: d:\mongo_storage\data" --logpath="a log path e.g: d:\mongo_storage\log\log.txt" --install --serviceName "MongoDB"

net start MongoDB

net stop MongoDB