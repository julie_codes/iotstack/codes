const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');
const sharp = require('sharp');
//const { IMAGENET_CLASSES } = require('./models/model5custom/inverted_class_indices.json');
const bodyParser = require('body-parser');
// Load the detection class indeces
const kerasLabels = require("./model/model5custom/inverted_class_indices.json"); //model2working
const { IMAGENET_CLASSES } = require('./imagenet_classes'); // objects
//const cocoLabels = { ...IMAGENET_CLASSES };
//const cocoLabels = require('@tensorflow-models/coco-ssd/dist/extension/coco_classes.json');
let labels = {};
  
async function processLabels(prediction, modelType) {
  let class_idxs = Array.from(tf.argMax(prediction).dataSync());

  switch (modelType) {
    case 'coco':
	  return class_idxs.map(() => ({ name: '' }));
      /*return class_idxs.map((class_idx) => {
        let predictedClass = cocoLabels[class_idx];
        return { name: predictedClass.displayName };
      });*/
    case 'inception':
      return class_idxs.map((class_idx) => {
        let predictedClass = imagenetLabels[class_idx];
        return { name: predictedClass };
      });
    case 'knn':
      return class_idxs.map((class_idx) => {
        let predictedDisease = knnClassifier.getClassExampleCount().find((entry) => entry.classIndex === class_idx).className;
        console.log(predictedDisease); // debugging statement
        /*let [name, disease] = predictedDisease.split('___');
        name = name.replaceAll('_', ' ');
        disease = disease.replaceAll('_', ' ');
        return { name, disease };*/
      });
    case 'keras':
      return class_idxs.map((class_idx) => {
        let predictedDisease = kerasLabels[class_idx];
        console.log(predictedDisease); // debugging statement
        /*let [name, disease] = predictedDisease.split('___');
        name = name.replaceAll('_', ' ');
        disease = disease.replaceAll('_', ' ');
        return { name, disease };*/
      });
    default:
      throw new Error(`Invalid model type: ${modelType}`);
  }
}


async function main() {
  // Create an Express app
 
	const app = express();
	app.use(cors());
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	let model = null;
  
  app.post('/set-model', async (req, res) => {
  const { modelName } = req.body;

  switch (modelName) {
    case 'coco':
      model = await cocoSsd.load('faster_rcnn_inception_v2');
	  //labels = cocoLabels;
	  console.log('coco model');
      break;
    case 'inception':
      model = await tf.loadGraphModel('https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1', { fromTFHub: true });
      labels = imagenetLabels;
	  console.log('inception model');
	  break;
    case 'knn':
      model = knnClassifier.create();
	  labels = null; 
	  console.log('knn model');
      break;
	case 'keras':
	  const kearsModelPath = 'models/model5custom/model.json';
	  model = await tf.loadLayersModel(`file://${kearsModelPath}`);
	  labels = kerasLabels;
	  console.log('keras model');
	  break;
    default:
      throw new Error(`Invalid model name: ${modelName}`);
  }

  console.log(`Model loaded: ${modelName}`);

  res.sendStatus(200);
});
  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });



  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });
  
  
  
  // Function to decode and preprocess image for COCO model
function decodeAndPreprocessImageForCOCO(img) {
  let imageTensor = tf.node.decodeImage(img);
  let offset = tf.scalar(255);
  let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
  let tensorImgScaled = tensorImg.div(offset);
  return tensorImgScaled;
}

// Function to decode and preprocess image for Inception model
function decodeAndPreprocessImageForInception(img) {
  let imageTensor = tf.node.decodeImage(img);
  let offset = tf.scalar(255);
  let tensorImg = imageTensor.resizeNearestNeighbor([299, 299]).toFloat().expandDims();
  let tensorImgScaled = tensorImg.sub(offset).div(offset);
  return tensorImgScaled;
}

// Function to decode and preprocess image for Keras model
function decodeAndPreprocessImageForKeras(img) {
  let imageTensor = tf.node.decodeImage(img);
  let offset = tf.scalar(255);
  let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
  let tensorImgScaled = tensorImg.div(offset);
  return tensorImgScaled;
}

 // Route to handle image prediction
app.post('/detect', upload.single('image'), async (req, res) => {
  try {
	  
	  
    // Load the model
    /*let modelPath = 'model/model5custom/model.json'; //note model2 working
    let model = await tf.loadLayersModel(`file://${modelPath}`);
    console.log('Model loaded');*/

    // Load image from file system
    const imagePath = req.file.path;
    let img = fs.readFileSync(imagePath);
    // Decode and preprocess image
    let imageTensor = tf.node.decodeImage(img);
    let offset = tf.scalar(255)
    let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
    let tensorImg_scaled = tensorImg.div(offset);
    // Predict keras custom
    let prediction = await model.predict(tensorImg_scaled).data();
    let predicted_class = tf.argMax(prediction);
    let class_idxs = Array.from(predicted_class.dataSync());
    let diseases = [];
	//Imagenet json
    for (let class_idx of class_idxs) {
      let predictedDisease = labels[class_idx];
      let [name, disease] = predictedDisease.split('___');
      name = name.replaceAll('_', ' ');
      disease = disease.replaceAll('_', ' ');
      diseases.push({
        name,
        disease,
      });
    }

    const base64Image = Buffer.from(img).toString('base64');
    const response = {
      img:base64Image,
      diseases:diseases
    };
    console.log(response);
    res.send(response); 
  } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while predicting image.');
  }
});


  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
