
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SensorDataSchema = new Schema({
  deviceId: {
    type: String,
    required: true,
  },
  msg: {
    type: {
      plantId: String,
      atmtemp: Number,
      atmhum: Number,
      atmheatindex: Number,
      soilmoisture: Number,
      soilsalt: Number,
      battlife: Number,
      soiltemp: Number,
      light: Number,
      location: {
        longitude: Number,
        latitude: Number,
      },
      timestamp: {
        type: Date,
        default: Date.now,
      },
      gateway: String,
      weather: {
        coord: {
          lon: Number,
          lat: Number,
        },
        weather: [
          {
            description: String,
            icon: String,
            id: Number,
            main: String,
          },
        ],
        base: String,
        main: {
          temp: Number,
          feels_like: Number,
          temp_min: Number,
          temp_max: Number,
          pressure: Number,
          humidity: Number,
        },
        visibility: Number,
        wind: {
          speed: Number,
          deg: Number,
        },
        clouds: {
          all: Number,
        },
        dt: Number,
        sys: {
          type: Number,
          id: Number,
          country: String,
          sunrise: Number,
          sunset: Number,
        },
        timezone: Number,
        id: Number,
        name: String,
        cod: Number,
        season: String,
      },
    },
    required: true,
    default: {},
  },
});

module.exports = mongoose.model('node01', SensorDataSchema);
