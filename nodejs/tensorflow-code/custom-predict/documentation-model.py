base_dir = './New Plant Diseases Dataset(Augmented)/New Plant Diseases Dataset(Augmented)'
os.listdir(base_dir)

len(os.listdir(os.path.join(base_dir, 'train')))

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cv2
import PIL
import tensorflow as tf
from tensorflow.python import keras
import warnings 
import argparse
warnings.filterwarnings('ignore')
#from tensorflow.python.keras.preprocessing.image import ImageDataGenerator, img_to_array, load_img
import tensorflowjs as tfjs
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.preprocessing import LabelBinarizer, StandardScaler
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
import os

fig,axes = plt.subplots(1,5, figsize=(18,18))
images = os.listdir(os.path.join(base_dir, 'train/Apple___Black_rot'))
for _ in range(5):
  ax = axes[_]
  image_path = base_dir+'/train/Apple___Black_rot/'+images[_]
  img = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
  ax.imshow(img)
  ax.axis('off')
plt.show()

train_datagen = ImageDataGenerator(rescale=1./255,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   width_shift_range=0.2,
                                   height_shift_range=0.2,
                                   fill_mode='nearest')

# Data augmentation for validation dataset
validation_datagen = ImageDataGenerator(rescale = 1./255)

classes_dict = train_set_from_dir.class_indices

## note make a file in folder called indicies/class_indices.json 
import json
with open('indicies/class_indices.json','w') as f:
  json.dump(classes_dict, f)
  
# Importing the required libraries for modelling 
from keras import Input, Model
from keras.applications import MobileNet
from keras.layers.core import Flatten, Dense,Dropout
from keras.layers import GlobalAveragePooling2D
#from keras.layers.normalization import BatchNormalization
from keras.layers import BatchNormalization
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.models import Model, model_from_json
from keras.optimizers import Adam
from sklearn.metrics import classification_report, roc_auc_score

base_model = MobileNet(
    #Load weights into the pre-trained MobileNet model
    weights="imagenet",
    input_shape=(224, 224, 3),
    #Exclude the ImageNet classifier at the top of the model
    include_top=False
)  

base_model.summary()

head_model = base_model.output
head_model = GlobalAveragePooling2D()(head_model)
# Regularization by applying DropOut
head_model = Dropout(0.2)(head_model)
outputs = Dense(38, activation="softmax")(head_model)
mobilenet_model = Model(base_model.input, outputs, name='pretrained_mobilenet' )

for layer in mobilenet_model.layers:
    layer.trainable = False

# or if we want to set the first 20 layers of the network to be non-trainable
for layer in  mobilenet_model.layers[:20]:
    layer.trainable=False
for layer in  mobilenet_model.layers[20:]:
    layer.trainable=True
	
# Compiling the model with the optimizer and loss function 

mobilenet_model.compile(optimizer = Adam(),
              loss = 'categorical_crossentropy',
              metrics = ['accuracy']
)

mobilenet_model.summary()
for idx, layer in enumerate(mobilenet_model.layers):
    print(idx, layer.name, layer.trainable)
	
## Setting up callbacks for our model

callbacks = [
           ModelCheckpoint('content/drive/My Drive/PLANT DISEASE RECOGNITION/checkpoints/mobilenet_plantdiseases.h5', save_best_only=True, monitor='val_acc'),
           EarlyStopping(monitor='val_loss', patience=2, verbose=1),
           ReduceLROnPlateau(factor=0.1, patience=10, min_lr=0.00001, verbose=1) 
]

N_EPOCHS = 10

history = mobilenet_model.fit(train_set_from_dir,
          validation_data = validation_set_from_dir,
          epochs = N_EPOCHS,
          # Use 128 random batches for training set 
          steps_per_epoch = 128, # 128 x 32 = 2**12 random samples
          # Use 64 random batches for training validation set
          validation_steps = 100, # 100 x 32 = 3200 random samples 
          callbacks = callbacks
          )

import matplotlib.pyplot as plt

n_epochs = 10

plt.figure(figsize = (8,5))
plt.plot(range(1, n_epochs+1), history.history['loss'], label = 'train_loss')
plt.plot(range(1, n_epochs+1), history.history['val_loss'], label = 'val_loss')
plt.plot(range(1, n_epochs+1), history.history['accuracy'], label = 'train_accuracy')
plt.plot(range(1, n_epochs+1), history.history['val_accuracy'], label = 'val_accuracy')
plt.title('Training and Validation Loss and Accuracy')
plt.xlabel('Epoch')
plt.ylabel('Loss / Accuracy')
plt.legend()
plt.show()

results = mobilenet_model.evaluate(validation_set_from_dir) 

mkdir plantmodel

mobilenet_model.save('plantmodel/mobilenet_model.h5')

# Testing on a random image from the test images directory
from PIL import Image
np.random.seed(200)
idx = np.random.randint(30)
test_images_dir = os.path.join('test', 'test')
test1 = Image.open(os.path.join(test_images_dir, os.listdir(test_images_dir)[idx]))

plt.imshow(test1)
plt.title(os.listdir(test_images_dir)[idx])

classes_dict = train_set_from_dir.class_indices
classes_dict = { v:k for (k,v) in classes_dict.items() }
classes_dict[np.argmax(predictions)]

!mkdir tensorflowjs-model 


!tensorflowjs_converter --input_format keras plantmodel/mobilenet_model.h5 tensorflowjs-model/

##creat my own indicides in the format they want from dataset folder

import json

# Load the class indices from file
with open('/content/drive/My Drive/PLANT DISEASE RECOGNITION/class_indices.json') as f:
    class_indices = json.load(f)

# Invert the dictionary
inverted_dict = {str(value): key for key, value in class_indices.items()}

# Save the inverted dictionary to file
with open('/content/drive/My Drive/PLANT DISEASE RECOGNITION/inverted_class_indices.json', 'w') as f:
    json.dump(inverted_dict, f)
