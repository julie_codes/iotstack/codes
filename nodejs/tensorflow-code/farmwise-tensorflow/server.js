const express = require('express');
const tf = require('@tensorflow/tfjs-node');
const fs = require('fs');
const app = express();

const cocoSsd = require('@tensorflow-models/coco-ssd');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const sharp = require('sharp');

// Load the detection class indeces
const labels = require("./model/model5custom/inverted_class_indices.json"); //model2working

// Enable cors
app.use(cors());

// Serve the HTML file with route
app.get('/', (req, res) => {
res.sendFile(__dirname + '/index.html');
});
	
  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });	

// Route to handle image prediction
app.post('/detect', upload.single('image'), async (req, res) => {
  try {
    // Load the model
    let modelPath = 'model/model5custom/model.json'; //note model2 working
    let model = await tf.loadLayersModel(`file://${modelPath}`);
    console.log('Model loaded');

    // Load image from file system
    const imagePath = req.file.path;
    let img = fs.readFileSync(imagePath);

    // Decode and preprocess image
    let imageTensor = tf.node.decodeImage(img);
    let offset = tf.scalar(255)
    let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
    let tensorImg_scaled = tensorImg.div(offset);
    
    let prediction = await model.predict(tensorImg_scaled).data();
    let predicted_class = tf.argMax(prediction);
    let class_idxs = Array.from(predicted_class.dataSync());
    let diseases = [];

    for (let class_idx of class_idxs) {
      let predictedDisease = labels[class_idx];
      let [name, disease] = predictedDisease.split('___');
      name = name.replaceAll('_', ' ');
      disease = disease.replaceAll('_', ' ');
      diseases.push({
        name,
        disease,
      });
    }

    const base64Image = Buffer.from(img).toString('base64');
    const response = {
      img:base64Image,
      diseases:diseases
    };
    console.log(response);
    res.send(response); 
  } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while predicting image.');
  }
});


// Start the server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
