const fs = require('fs');
const fsp = require('fs').promises;
const multer = require('multer');
const path = require('path');
const WebSocket = require('ws');
const cors = require('cors');
//const sharp = require('sharp');
var host = process.env.IP || 'localhost';
const express = require('express')
const app = express()
const mongoose = require('mongoose')
var mqtt = require('mqtt')
const FormData = require('form-data');
//var url = "mongodb://localhost:27017/"; //mongodb+srv://juliesundar:HELENKELLER1!@cluster0.pnrsquj.mongodb.net/farmbuddies
//var url = "mongodb+srv://juliesundar:HELENKELLER1!@cluster0.pnrsquj.mongodb.net/farmbuddies";
var MongoClient = require('mongodb').MongoClient;
const url = 'mongodb+srv://farmwise:farmwise@farmwise.fxvafug.mongodb.net/farmbuddies?retryWrites=true&w=majority';

var staticSite = __dirname + '/public';

// Load the detection class indeces
const labels = require("./model/model5custom/inverted_class_indices.json"); //model2working

//Gpio topic 
const statusTopic = 'farmwise/node01/gpio/status';
// Create an array to store video frames
const videoFrames = [];

// Set up a folder to save images
const imagesFolder = path.join(__dirname, 'public', 'images');
//const imagesFolder = path.join(__dirname, 'images');
if (!fs.existsSync(imagesFolder)) {
  fs.mkdirSync(imagesFolder);
}

app.use(express.raw({ type: 'image/jpeg', limit: '10mb' }));

// Enable cors
app.use(cors());

  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });	


//const WebSocket = require('ws');
const http = require('http');


 // Create an HTTP server
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('WebSocket Server\n');
});

// Combine the WebSocket and HTTP server
const wss = new WebSocket.Server({ server });


// Initialize an array to keep track of connected WebSocket clients
const clients = [];

// WebSocket server event handlers
wss.on('connection', (ws) => {
  console.log('Client connected');
  //const binaryData = /* Your binary data as a Buffer or ArrayBuffer */;
  //
  
  // Add the connected WebSocket client to the array
  clients.push(ws);

  // Handle messages from clients
  ws.on('message', (message) => {
  // Loop through all connected clients
  clients.forEach((client) => {
    // Check if the client is in an open state
    if (client.readyState === WebSocket.OPEN) {
      // Send the binary data to the client
      client.send(message, { binary: true });
    }
  });
  
  });

  // Handle disconnection
  ws.on('close', () => {
    console.log('Client disconnected');
    
    // Remove the disconnected WebSocket client from the array
    const index = clients.indexOf(ws);
    if (index !== -1) {
      clients.splice(index, 1);
    }
  });
});


mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('Connected to Database'))

app.use(express.json())

//var client  = mqtt.connect([{host:'localhost',port:'9001'}]) //var client  = mqtt.connect([{host:'broker.hivemq.com',port:'1883'}]) //
const client = mqtt.connect('ws://broker.hivemq.com:8000/mqtt'); // Adjust MQTT broker URL

 console.log("Nodejs Server Started!");


client.on('connect', function () {
   client.subscribe([
    //'farmwise/node01/sensors/data',
    //'farmwise/node01/gpio/status',
    //'farmwise/node01/gpio/control',
    //'farmwise/node01/stream',
    //'FarmBuddy/frontgarden/node01',
    'farmwise/node01/sensors/data'
  ], function (err) {
	  console.log("farmwise topics");
      if(err)
      console.log(err)
  })
})

const axios = require('axios');

// Function to convert temperature from Kelvin to Celsius
const kelvinToCelsius = (tempKelvin) => {
  return tempKelvin - 273.15;
};

// Function to get weather data from OpenWeatherMap API
const getWeatherData = async () => {
  try {
    const city = 'Paramaribo'; // Adjust the city as needed

    const response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=Paramaribo&APPID=6185be2ed84ee299aaea2515c1a63684`);
    const weatherData = response.data;

    // Convert temperature values to Celsius
    weatherData.main.temp = kelvinToCelsius(weatherData.main.temp);
    weatherData.main.feels_like = kelvinToCelsius(weatherData.main.feels_like);
    weatherData.main.temp_min = kelvinToCelsius(weatherData.main.temp_min);
    weatherData.main.temp_max = kelvinToCelsius(weatherData.main.temp_max);
    
    // Extract timestamp from weather data
    const timestamp = weatherData.dt * 1000; // Convert seconds to milliseconds

    // Add season information to the weatherData object
    weatherData.season = getSeason(timestamp);
    
    return weatherData;
  } catch (error) {
    console.error('Error fetching weather data:', error.message);
    throw error;
  }
};


// Function to get the season based on the timestamp
const getSeason = (timestamp) => {
  // Define the start and end timestamps for each season in Suriname (in milliseconds)
  const minorRainyStart = new Date('December 1, 2023 00:00:00 GMT-0300').getTime();
  const minorRainyEnd = new Date('February 1, 2024 00:00:00 GMT-0300').getTime();
  const minorDryStart = new Date('February 1, 2024 00:00:00 GMT-0300').getTime();
  const minorDryEnd = new Date('April 30, 2024 23:59:59 GMT-0300').getTime();
  const majorRainyStart = new Date('April 30, 2024 23:59:59 GMT-0300').getTime();
  const majorRainyEnd = new Date('August 15, 2024 23:59:59 GMT-0300').getTime();
  const majorDryStart = new Date('August 15, 2024 23:59:59 GMT-0300').getTime();
  const majorDryEnd = new Date('December 1, 2024 00:00:00 GMT-0300').getTime();

  // Convert the input timestamp to a JavaScript Date object
  const date = new Date(timestamp);

  // Check if the date falls within each season's timestamp range
  if (date >= minorRainyStart && date < minorRainyEnd) {
    return 'Minor Rainy Season';
  } else if (date >= minorDryStart && date < minorDryEnd) {
    return 'Minor Dry Season';
  } else if (date >= majorRainyStart && date < majorRainyEnd) {
    return 'Major Rainy Season';
  } else {
    return 'Major Dry Season';
  }
};




app.get('/control-gpio', function (req, res) {
  // Extract GPIO pin and command from the query parameters
  const { pin, command } = req.query;

  // Publish GPIO control command to the MQTT topic
  client.publish('farmwise/node01/gpio/control', JSON.stringify({ pin, command }), function (err) {
    if (err) {
      console.error('Failed to publish GPIO control command:', err);
      res.status(500).send('Failed to control GPIO');
    } else {
      console.log('GPIO control command published:', pin, command);
      res.status(200).send('GPIO control command sent');
    }
  });
});

 
// Handle MQTT messages
/* client.on('message', function (topic, message) {
  console.log('Received MQTT message on topic:', topic);
  console.log('Message:', message.toString());
  // Rest of your message handling code...
  console.log('Received video frame on topic:', topic);
  console.log('Message length:', message.length);
  if (topic === statusTopic) {
    console.log('Received status update:', message.toString());
  } else if (topic === 'farmwise/node01/stream') {
    // Broadcast video frames to WebSocket clients
    videoFrames.push(message);
    console.log(message);
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
        console.log(message);
      }
    });
  } else {
    // Handle other MQTT topics as needed
  }
});  */

////////////////COMBINE BOTH CLIENT. MESSSAGE
// Handle MQTT messages
client.on('message', async function (topic, message) {
  console.log(message);
  try {
      
  console.log('Received MQTT message on topic:', topic);
  console.log('Message:', message.toString());
  // Rest of your message handling code...
  console.log('Received video frame on topic:', topic);
  console.log('Message length:', message.length);
  if (topic === statusTopic) {
    console.log('Received status update:', message.toString());
  } else if (topic === 'farmwise/node01/stream') {
    // Broadcast video frames to WebSocket clients
    videoFrames.push(message);
    console.log(message);
    wss.clients.forEach((client) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
        console.log(message);
      }
    });
  } else {
    // Handle other MQTT topics as needed
  }
      
    console.log('Received MQTT message. Length:', message.length);
    
    const incomingMsg = JSON.parse(message.toString());
    
    // Log some details about the received message
    console.log('Received Message Details:');
    console.log('Topic:', topic);
    console.log('Payload:', incomingMsg);
    
     // Add timestamp and gateway information
    incomingMsg.timestamp = new Date();
    //incomingMsg.timestamp = new Date().toLocaleString("en-US", { timeZone: "America/Paramaribo" });
    incomingMsg.gateway = "FarmBuddy_Gateway01";
    
    // Get weather data
    const weatherData = await getWeatherData();

    // Combine incoming MQTT message and weather data
    const combinedMsg = {
      ...incomingMsg,
      weather: weatherData,
    };

    // Insert combined message into MongoDB
    //Mongo_insert(combinedMsg);
    Mongo_insert(sensorCollectionName, combinedMsg);

    console.log('Combined message stored in MongoDB:', combinedMsg);
  } catch (error) {
    console.error('Error processing MQTT message:', error.message);
  }
}); 


// Check if data is JSON or not
function json_check(data) {
  try {
    const msg = JSON.parse(data.toString());
    
    const msgobj = { "msg": msg }; // message object
    Mongo_insert(msgobj);
    console.log(msgobj);
  } catch (e) {
    console.log("Message is not valid JSON: " + data.toString());
    return false;
  }
}


// Example usage for storing sensor data
const sensorData = { /* Your sensor data */ };
const sensorCollectionName = "node03";


// Example usage for storing image data
const imageData = { /* Your image data */ };
const imageCollectionName = "image_data";


// Insert data into MongoDB Atlas
function Mongo_insert(collectionName, msg) {
   MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
   //MongoClient.connect(localMongo, { useUnifiedTopology: true }, function (err, client) {
    if (err) throw err;
    const dbo = client.db("farmbuddies");
    dbo.collection(collectionName).insertOne(msg, function (err, res) {
    //dbo.collection("image_data").insertOne(msg, function (err, res) {
      if (err) throw err;
      console.log("Data stored");
      console.log("Data stored in collection:", collectionName);
      client.close();
    });
  });
}

const exif = require('exif').ExifImage; // Assuming you have the 'exif' library installed

const exifParser = require('exif-parser');

app.post('/upload', async (req, res) => {
  try {
    const imageBuffer = req.body;

    if (!imageBuffer) {
      throw new Error('No image file provided');
    }

    // Generate a timestamp and format it for human readability
    const humanReadableTimestamp = new Date().toLocaleString().replace(/:/g, '-').replace(/\//g, '_').replace(/,/g, '');

    // Save the image to a file in the "images" folder with human-readable timestamp in the name
    const imagePath = path.join(imagesFolder, `${humanReadableTimestamp}_image.jpg`);

    // Extract the computer-coded timestamp using your own logic
    let computerCodedTimestamp = new Date(); // Replace with your actual logic to get the computer-coded timestamp
    console.log(computerCodedTimestamp);
    // Add timestamp gateway and URL info to the metadata
    const metadata = {
      gateway: 'Gateway_01',
      timestamp: computerCodedTimestamp.toISOString(),
      localPath: imagePath
      //imageUrl: `C:\nodejs\projects\latest-rest\images/${humanReadableTimestamp}_image.jpg`,
    };
    console.log(metadata);
    // Save the image buffer to the file
    fs.writeFileSync(imagePath, imageBuffer);

    // Embed the metadata in the image as a comment using exif-parser
    const parser = exifParser.create(imageBuffer);
    const exifData = parser.parse();
    const existingComment = exifData.tags.Comment || '';
    const newComment = `${existingComment}\n${JSON.stringify(metadata)}`;

    // Write the new comment to the image file
    fs.writeFileSync(imagePath, imageBuffer, { flag: 'w' });
    fs.appendFileSync(imagePath, newComment, { encoding: 'utf-8' });

    console.log(`Image received. Size: ${imageBuffer.length} bytes. Saved at: ${imagePath}`);
    console.log(`Computer-coded timestamp and metadata embedded in image.`);

    //await Mongo_insert(metadata);
    await Mongo_insert(imageCollectionName, metadata);

    res.status(200).json({ message: 'Image received and saved successfully' });
  } catch (error) {
    console.error('Error processing image upload:', error);
    res.status(400).json({ error: error.message || 'Bad request' });
  }
});

const storage = multer.memoryStorage();
const upload2 = multer({ storage: storage });

// Handle file upload
app.post('/prediction', upload2.single('image'), async (req, res) => {
  // Access the uploaded file via req.file
  if (!req.file) {
    return res.status(400).send('No file uploaded.');
  }

  // Prepare FormData to send image to Flask server
  const formData = new FormData();
  formData.append('image', req.file.buffer, { filename: req.file.originalname });

  try {
    // Make a POST request to Flask server
    const flaskResponse = await axios.post('http://localhost:5000/predict', formData, {
      headers: {
        ...formData.getHeaders(), // Set content-type header with boundary
      },
    });

    // Log Flask server response
    console.log('Flask server response:', flaskResponse.data);

    // Send a response
    res.send('File uploaded successfully.');
  } catch (error) {
    console.error('Error sending image to Flask server:', error.message);
    res.status(500).send('Internal server error.');
  }
});

app.post('/live_stream', async (req, res) => {
  // Handle the live stream data here
  // For simplicity, just log the data to the console
  console.log('Received live stream data');

  // Respond to the client
  res.status(200).send('Live stream data received');
});

const subscribersRouter = require('./routes/subscribers')
app.use('/data', subscribersRouter)

// ENABLE CORS for Express (so swagger.io and external sites can use it remotely .. SECURE IT LATER!!)
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Routes here

app.use('/', express.static(staticSite));


// Serve the HTML file with route
app.get('/', (req, res) => {
res.sendFile(__dirname + '/index.html');
});

app.get('/carosel', (req, res) => {
    res.sendFile(path.join(__dirname, 'carosel.html'));
});


app.get('/getImages', async (req, res) => {
    try {
        //const imagesPath = path.join(__dirname, './images');
        const imagesPath = path.join(__dirname, 'public', 'images');
        //const imagesPath = path.join(__dirname, 'images');
        const imageFileNames = await fsp.readdir(imagesPath);

        res.json(imageFileNames);
    } catch (error) {
        console.error('Error reading images:', error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
});
// Start the HTTP server on port 3000
const PORT = 4000;
server.listen(PORT, () => {
  console.log(`websocket listenisng on port ${PORT}`);
});

app.listen(7000, () => console.log('Server Started port 3000'))