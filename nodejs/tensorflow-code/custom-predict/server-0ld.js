const express = require('express');
const tf = require('@tensorflow/tfjs-node');
const fs = require('fs');
const app = express();



// Route to handle image prediction
app.get('/predict', async (req, res) => {
  try {
// Load the model
const modelPath = 'model/tfjs_model/model.json';
const model = tf.loadLayersModel(`file://${modelPath}`);
console.log(model);
console.log("model loaded");
    // Load image from file system
    const imagePath = 'image/image.JPG';
	console.log("imagePath");
    const imageBuffer = fs.readFileSync(imagePath);

    // Decode and preprocess image
    const imageTensor = tf.node.decodeImage(imageBuffer);
    const preprocessedImage = tf.cast(tf.image.resizeBilinear(imageTensor, [224, 224]), 'float32').div(255);
    //const preprocessedImage = tf.cast(tf.image.resizeBilinear(imageTensor, [224, 224]), 'float32').div(255).expandDims();
	//const preprocessedImage = tf.cast(tf.image.resizeBilinear(imageTensor, [256, 256]), 'float32').div(255);
	//const inputTensor = tf.expandDims(preprocessedImage, 0);
    // Make prediction using model
    const prediction = await model.then((model) => model.predict(preprocessedImage.reshape([1, 224, 224, 3])));
	//const prediction = await model.then((model) => model.predict(preprocessedImage.reshape([1, 256, 256, 3])));
    const predictedClassIndex = prediction.argMax(axis=1).dataSync()[0];
	const classNames = ['Potato___Early_blight', 'Potato___healthy', 'Potato___Late_blight'];
	const predictedClassName = classNames[predictedClassIndex];
	console.log('prediction:' ,predictedClassName);
    // Get the confidence score
	const confidence = tf.max(prediction).dataSync()[0] * 100;

	// Log the prediction result
	console.log(`Predicted class: ${predictedClassName}, Confidence: ${confidence.toFixed(2)}%`);

    // Send predicted class index as response to client
    res.send(`Predicted class index: ${predictedClassIndex}`);
  } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while predicting image.');
  }
});

// Start the server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
