const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
//const yolo = require('@tensorflow-models/yolo');
//const tinyYoloV3 = require('tiny-yolov3');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');
const sharp = require('sharp');
const bodyParser = require('body-parser');

// Load the detection class indeces
const kerasLabels = require("./model/model5custom/inverted_class_indices.json"); //model2working
const { IMAGENET_CLASSES } = require('./imagenet_classes'); // objects

let labels = {};
let selectedModelName = '';

async function processLabels(prediction, modelType) {
  let class_idxs = Array.from(tf.argMax(prediction).dataSync());

  switch (modelType) {
    case 'coco':
	  return class_idxs.map(() => ({ name: '' }));
      /*return class_idxs.map((class_idx) => {
        let predictedClass = cocoLabels[class_idx];
        return { name: predictedClass.displayName };
      });*/
    case 'inception':
      /*return class_idxs.map((class_idx) => {
        let predictedClass = imagenetLabels[class_idx];
        return { name: predictedClass };
      });*/
    case 'knn':
      return class_idxs.map((class_idx) => {
        let predictedDisease = knnClassifier.getClassExampleCount().find((entry) => entry.classIndex === class_idx).className;
        console.log(predictedDisease); // debugging statement
        /*let [name, disease] = predictedDisease.split('___');
        name = name.replaceAll('_', ' ');
        disease = disease.replaceAll('_', ' ');
        return { name, disease };*/
      });
    case 'keras':
      return class_idxs.map((class_idx) => {
        let predictedDisease = kerasLabels[class_idx];
        console.log(predictedDisease); // debugging statement
        /*let [name, disease] = predictedDisease.split('___');
        name = name.replaceAll('_', ' ');
        disease = disease.replaceAll('_', ' ');
        return { name, disease };*/
      });
    default:
      throw new Error(`Invalid model type: ${modelType}`);
  }
}




let model = null;

async function main() {
  // Create an Express app
 
	const app = express();
	app.use(cors());
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	//let model = null;
  
  app.post('/set-model', async (req, res) => {
  const { modelName } = req.body;

  switch (modelName) {
    case 'coco':
      //model = await cocoSsd.load({ base: 'lite_mobilenet_v2' }); // works 15 seconds
      model = await cocoSsd.load('faster_rcnn_inception_v2');
      //model = await cocoSsd.load(); // works 17 seconds
     // model = await cocoSsd.load({base: 'mobilenet_v2'}); //works 58 seconds
	  //labels = cocoLabels;
	  console.log('coco model');
      break;
    case 'inception':
      model = await tf.loadGraphModel('https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1', { fromTFHub: true });
      //labels = imagenetLabels;
	  console.log('inception model');
	  break;
    case 'knn':
      model = knnClassifier.create();
	  labels = null; 
	  console.log('knn model');
      break;
	case 'keras':
	  const kearsModelPath = 'models/model5custom/model.json';
	  model = await tf.loadLayersModel(`file://${kearsModelPath}`);
	  labels = kerasLabels;
	  console.log('keras model');
	  break;
    default:
      throw new Error(`Invalid model name: ${modelName}`);
  }

  console.log(`Model loaded: ${modelName}`);
  
  selectedModelName = modelName;

  res.sendStatus(200);
  //res.sendStatus(modelName);
  
});
  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });



  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });
  
// Define the function for each model
async function predictDiseaseCoco(req, res, imagePath, modelName){
    const imageBuffer = fs.readFileSync(imagePath);
    
    // Resize the image to 800x800
    const image = await sharp(imageBuffer).resize(800, 800).toBuffer();
    console.log('Resized image shape:', await sharp(image).metadata());
  
    const decodedImage = tf.node.decodeImage(image, 3);
    console.log(decodedImage.shape);
    const predictions = await model.detect(decodedImage);
    console.log('Predictions:', predictions);
    
    const boxes = [];

    // Check that predictions is not empty and has a bbox property
    if (predictions && predictions.length > 0 && predictions[0].hasOwnProperty('bbox')) {

      // Loop through the predictions and push the bounding box information to the array
      for (const prediction of predictions) {
        const [ymin, xmin, ymax, xmax] = prediction.bbox;
        boxes.push({
          ymin: ymin * decodedImage.shape[0] / 800,
          xmin: xmin * decodedImage.shape[1] / 800,
          ymax: ymax * decodedImage.shape[0] / 800,
          xmax: xmax * decodedImage.shape[1] / 800,
          class: prediction.class
        });
      }
    } else {
      // Handle the case where predictions is empty or doesn't have a bbox property
      console.error('Invalid predictions');
    }

    // Construct the response object with the base64 encoded image and bounding boxes information
    const base64Image = Buffer.from(image).toString('base64');
    const response = {
      image: base64Image,
      boxes: boxes,
      predictions:predictions
    };
     res.send(response);
     //return response;
     console.log(response);
}

async function predictDiseaseInception(req, res, imagePath, modelName) {

  const classifier = knnClassifier.create();
  const imageBuffer = fs.readFileSync(imagePath);
    
  // Preprocess the image
  const preprocessImage = (imageBuffer) => {
    const tfImage = tf.node.decodeImage(imageBuffer, 3); // adjst based on model
    //const tfResizedImage = tf.image.resizeBilinear(tfImage, [128, 128]); // adjust based on model
	const tfResizedImage = tf.image.resizeBilinear(tfImage, [299, 299]);
    const tfFloatImage = tf.cast(tfResizedImage, 'float32');
    const tfScaledImage = tfFloatImage.div(tf.scalar(255));
    const tfBatchedImage = tfScaledImage.expandDims(0);
    return tfBatchedImage;
  };
  
  // Classify the image using the model
  const classifyImage = async (imageBuffer) => {
    const tfBatchedImage = preprocessImage(imageBuffer);
    const prediction = await model.predict(tfBatchedImage);
    const values = await prediction.data();
    const index = values.indexOf(Math.max(...values));
    const className = IMAGENET_CLASSES[index];
    return { className, index };
  };


  const predictedClassIndex = await classifyImage(imageBuffer);
  console.log('Classification Results:', predictedClassIndex);
  res.send({ predictedClassIndex });
  
}

async function predictDiseaseKnn(req, res, imagePath, modelName) {
  // Implementation for the KNN model
}


async function predictDiseaseKeras(req, res, imagePath, modelName) {
  try {
    // Load image from file system
    let img = fs.readFileSync(imagePath);
    // Decode and preprocess image
    let imageTensor = tf.node.decodeImage(img);
    let offset = tf.scalar(255)
    let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
    let tensorImg_scaled = tensorImg.div(offset);
    // Predict keras custom
    let prediction = await model.predict(tensorImg_scaled).data();
    let predicted_class = tf.argMax(prediction);
    let class_idxs = Array.from(predicted_class.dataSync());
    let diseases = [];
    //Imagenet json
    for (let class_idx of class_idxs) {
      let predictedDisease = labels[class_idx];
      let [name, disease] = predictedDisease.split('___');
      name = name.replaceAll('_', ' ');
      disease = disease.replaceAll('_', ' ');
      diseases.push({
        name,
        disease,
      });
    }
    const base64Image = Buffer.from(img).toString('base64');
    const response = {
      img:base64Image,
      diseases:diseases
    };
    console.log(response);
    res.send(response);
    //return response;
  } catch (error) {
    console.error(error);
    throw new Error('An error occurred while predicting image.');
  }
}


// Route to handle image prediction
app.post('/detect', upload.single('image'), async (req, res) => {
  try {
    // Load image from file system
    const imagePath = req.file.path;
    let img = fs.readFileSync(imagePath);

    // Get the selected model name from the request
    const modelName = req.body.model;
    console.log(modelName);

    // Call the appropriate prediction function based on the selected model name
    let prediction;
    if (selectedModelName === 'coco') {
      prediction = await predictDiseaseCoco(req, res, imagePath, modelName);
    } else if (selectedModelName === 'inception') {
      prediction = await predictDiseaseInception(req, res, imagePath, modelName);
    } else if (selectedModelName === 'knn') {
      prediction = await predictDiseaseKnn(req, res, imagePath, modelName);
    } else if (selectedModelName === 'keras') {
      prediction = await predictDiseaseKeras(req, res, imagePath, modelName);
    } else {
      throw new Error(`Unknown model name: ${modelName}`);
    }
	
   // const response = await predictDiseaseKeras(imagePath);
    res.send(prediction);
    
     } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while predicting image.');
  }
});


  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
