const express = require('express');
const multer  = require('multer');
const axios = require('axios'); // Import axios library
const FormData = require('form-data');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3000;

// Multer storage configuration
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

// Serve the HTML file
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// Handle file upload
app.post('/upload2', upload.single('image'), async (req, res) => {
  // Access the uploaded file via req.file
  if (!req.file) {
    return res.status(400).send('No file uploaded.');
  }

  // Prepare FormData to send image to Flask server
  const formData = new FormData();
  formData.append('image', req.file.buffer, { filename: req.file.originalname });

  try {
    // Make a POST request to Flask server
    const flaskResponse = await axios.post('http://localhost:5000/predict', formData, {
      headers: {
        ...formData.getHeaders(), // Set content-type header with boundary
      },
    });

    // Log Flask server response
    console.log('Flask server response:', flaskResponse.data);

    // Send a response
    res.send('File uploaded successfully.');
  } catch (error) {
    console.error('Error sending image to Flask server:', error.message);
    res.status(500).send('Internal server error.');
  }
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
