
import tensorflow as tf
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Flatten, Dense
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflowjs as tfjs

# Define the combined dataset directory
dataset_dir = 'combined_dataset'

# Define data augmentation and preprocessing parameters
datagen = ImageDataGenerator(
    rescale=1./255,
    rotation_range=20,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest'
)

# Load and preprocess the dataset
train_data = datagen.flow_from_directory(
    dataset_dir,
    target_size=(224, 224),
    batch_size=32,
    class_mode='categorical',  # Set class_mode to 'categorical' for two classes
    shuffle=True
)


# Load the VGG16 model without the top classification layer
vgg16_model = VGG16(weights='imagenet', include_top=False, input_shape=(224, 224, 3))

# Freeze the weights of the VGG16 model
for layer in vgg16_model.layers:
    layer.trainable = False

# Add a custom classification head on top of the VGG16 model
last_layer = vgg16_model.output
flatten = Flatten()(last_layer)
dense1 = Dense(512, activation='relu')(flatten)
output = Dense(2, activation='softmax')(dense1)  # Use softmax activation for binary classification

# Create the custom model
custom_model = Model(inputs=vgg16_model.input, outputs=output)

# Compile the model
custom_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])  # Use binary_crossentropy for binary classification

# Train the model
num_epochs = 10
custom_model.fit(train_data, epochs=num_epochs)

# Save the model in TensorFlow.js format
tfjs.converters.save_keras_model(custom_model, './tfjs_model')

# Save the model in .h5 format
custom_model.save('./model.h5')
