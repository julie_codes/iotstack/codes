require('dotenv').config();

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const mqtt = require('mqtt');
const http = require('http').createServer(app);
const io = require('socket.io')(http);

// MongoDB connection
mongoose.connect('mongodb://localhost:27017/farmbuddies', { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to Database'));

// MQTT connection
const client = mqtt.connect([{ host: 'localhost', port: '1883' }]);
console.log('Nodejs Server Started!');

// Subscribe to 'juliesundar/codettes/01' topic
client.on('connect', function () {
  client.subscribe('juliesundar/codettes/01', function (err) {
    console.log('sub scribing to test topic');
    if (err) {
      console.log(err);
    }
  });
});

// When message received, check if it's valid JSON and insert it into MongoDB
client.on('message', function (topic, message) {
  try {
    const msg = JSON.parse(message.toString());
    msg.timestamp = new Date();
    msg.gateway = 'FarmBuddy_Gateway01';
    console.log(msg);
    Mongo_insert({ msg });
  } catch (e) {
    console.log('message could not be parsed as JSON');
    return false;
  }
});

// Insert data into MongoDB
function Mongo_insert(data) {
  const Subscriber = require('./models/subscriber');
  const subscriber = new Subscriber({
    temperature: data.msg.temperature,
    humidity: data.msg.humidity,
    deviceId: data.msg.deviceId,
    msg: data.msg
  });

  subscriber.save((error) => {
    if (error) {
      console.log('Error saving data:', error.message);
    } else {
      console.log('Data saved:', data);
    }
  });
}

// Enable CORS for Express
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// API routes
const subscribersRouter = require('./routes/subscribers');
app.use('/data', subscribersRouter);

// Serve static files
const staticSite = __dirname + '/public';
app.use('/', express.static(staticSite));

// Start server
const port = process.env.PORT || 3000;
http.listen(port, () => console.log(`Server started on port ${port}`));

// Get data from MongoDB based on start and end date
/*
app.get('/data/:start/:end', function (req, res) {
  const Subscriber = require('./models/subscriber');
  const startDate = new Date(req.params.start);
  const endDate = new Date(req.params.end);

  Subscriber.find({ timestamp: { $gte: startDate, $lte: endDate } }, function (err, data) {
    if (err) {
      console.log(err);
      res.status(500).send(err);
    } else {
      console.log(data);
      res.json(data);
    }
  });
});*/
