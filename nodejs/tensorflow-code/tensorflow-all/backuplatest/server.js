const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');
const sharp = require('sharp');
const { IMAGENET_CLASSES } = require('./imagenet_classes');

async function main() {
  // Load the Inception model for image classification
  const inceptModelPath = 'https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1';
  const inceptModel = await tf.loadGraphModel(inceptModelPath, { fromTFHub: true });
  console.log('Inception model loaded');

  // Load the COCO-SSD model for pest detection
  const cocoModel = await cocoSsd.load('faster_rcnn_inception_v2');
  console.log('COCO-SSD model loaded');

  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });

  // Create an Express app
  const app = express();
  app.use(cors());

  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

  // Handle HTTP POST requests to /detect
  app.post('/detect', upload.single('image'), async (req, res) => {
    try {
      // Read and resize the image
      const imageBuffer = fs.readFileSync(req.file.path);
      const image = await sharp(imageBuffer).resize(800, 800).toBuffer();

      // Classify the image using the Inception model
      const tfImage = tf.node.decodeImage(image, 3);
      //console.log('tfImage:', tfImage); //Tensor
      const tfResizedImage = tf.image.resizeBilinear(tfImage, [299, 299]);
      const tfFloatImage = tf.cast(tfResizedImage, 'float32');
      const tfScaledImage = tfFloatImage.div(tf.scalar(255));
      const tfBatchedImage = tfScaledImage.expandDims(0);
      const prediction = await inceptModel.predict(tfBatchedImage);
       console.log('Inception Predict:', prediction);
      const values = await prediction.data();
      //console.log('values:', values); // Float32Array
      const index = values.indexOf(Math.max(...values));
      // console.log('index:', index); //990
      const vegetables = ['carrot', 'corn', 'lettuce', 'pepper', 'tomato'];
      
      let predictedVegetable;
      if (index >= 0 && index < vegetables.length) {
        predictedVegetable = vegetables[index];
        console.log('predictedVegetables:',predictedVegetable);
      } else {
        predictedVegetable = 'unknown';
      }
      
      // Detect pests using the COCO-SSD model
      const decodedImage = tf.node.decodeImage(image, 3);
      const predictions = await cocoModel.detect(decodedImage);
      console.log('Coco Predictions:', predictions); //Apple
      const pestsPresent = predictions.length > 0;
      const base64Image = Buffer.from(image).toString('base64');

         const boxes = [];
            // Check that predictions is not empty and has a bbox property
            if (predictions && predictions.length > 0 && predictions[0].hasOwnProperty('bbox')) {

              // Loop through the predictions and push the bounding box information to the array
              for (const prediction of predictions) {
                const [ymin, xmin, ymax, xmax] = prediction.bbox;
                boxes.push({
                  ymin: ymin * decodedImage.shape[0] / 800,
                  xmin: xmin * decodedImage.shape[1] / 800,
                  ymax: ymax * decodedImage.shape[0] / 800,
                  xmax: xmax * decodedImage.shape[1] / 800,
                  class: prediction.class
                });
              }
            } else {
              // Handle the case where predictions is empty or doesn't have a bbox property
              console.error('Invalid predictions');
            }
      
      // Send the response
      const response = {
        image: base64Image,
        vegetable: predictedVegetable,
        pestsPresent: pestsPresent,
        boxes : boxes
      };
      res.send(response);
      console.log(response);
      
    } catch (err) {
      console.error(err);
      res.status(500).send('Error detecting pests');
    }
  });


  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
