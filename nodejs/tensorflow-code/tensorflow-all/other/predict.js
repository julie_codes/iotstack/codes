const http = require('http');
const formidable = require('formidable');
const fs = require('fs');
const tf = require('@tensorflow/tfjs-node');

const server = http.createServer((req, res) => {
  if (req.url == '/upload' && req.method.toLowerCase() == 'post') {
    // parse a file upload
    const form = new formidable.IncomingForm();

    form.parse(req, (err, fields, files) => {
      if (err) {
        console.error('Error parsing upload', err);
        res.writeHead(500, {'Content-Type': 'text/plain'});
        res.end('Internal server error');
        return;
      }

      console.log('Fields:', fields);
      console.log('Files:', files);

      // Move uploaded file to ./uploads directory
      const filePropName = Object.keys(files)[0];
      const uploadedFile = files[filePropName];
      const oldPath = uploadedFile.path;
      const newDir = './uploads/';
      const newPath = newDir + uploadedFile.name;

      // Create ./uploads directory if it doesn't exist
      if (!fs.existsSync(newDir)){
          fs.mkdirSync(newDir);
      }

      fs.rename(oldPath, newPath, function (err) {
        if (err) {
          console.error('Error moving uploaded file', err);
          res.writeHead(500, {'Content-Type': 'text/plain'});
          res.end('Internal server error');
          return;
        }

        // Load the model and make a prediction
        const model = tf.loadLayersModel('file://' + __dirname + '/model/model.json');

        const img = fs.readFileSync(newPath);
        const tensorImg = tf.node.decodeImage(img, 3);
        const batchedImg = tensorImg.expandDims(0);

        model.then(model => {
          const prediction = model.predict(batchedImg);
          const response = Array.from(prediction.dataSync());
          res.writeHead(200, {'Content-Type': 'application/json'});
          res.end(JSON.stringify(response));
        }).catch(err => {
          console.error('Error making prediction', err);
          res.writeHead(500, {'Content-Type': 'text/plain'});
          res.end('Internal server error');
        });
      });
    });
    return;
  }

  // Display a form for uploading files
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end(`
    <h1>Upload an image</h1>
    <form action="/upload" method="post" enctype="multipart/form-data">
      <input type="file" name="image"><br><br>
      <input type="submit" value="Upload">
    </form>
  `);
});

const port = 8080;
server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
