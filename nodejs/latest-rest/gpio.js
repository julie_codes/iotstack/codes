const express = require('express');
const bodyParser = require('body-parser');
const mqtt = require('mqtt');
const path = require('path'); // Add this line for handling file paths

const app = express();
const port = 3000;

// Mock data for nodes and pins
const nodes = ['Node1', 'Node2', 'Node3'];
const pins = {
  'Node1': ['GPIO1', 'GPIO2', 'GPIO3'],
  'Node2': ['GPIO4', 'GPIO5', 'GPIO6'],
  'Node3': ['GPIO7', 'GPIO8', 'GPIO9'],
};

// MQTT broker setup
const mqttBroker = 'mqtt://your-mqtt-broker-url'; // Replace with your MQTT broker URL
const client = mqtt.connect(mqttBroker);

// Middleware for parsing JSON data
app.use(bodyParser.json());

// Serve the index.html file
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'));
});

// Endpoint to get available nodes
app.get('/get-nodes', (req, res) => {
  res.json(nodes);
});

// Endpoint to get available pins for a node
app.get('/get-pins/:node', (req, res) => {
  const selectedNode = req.params.node;
  const selectedPins = pins[selectedNode] || [];
  res.json(selectedPins);
});

// Endpoint to control GPIO
app.post('/control-gpio', (req, res) => {
  const { node, pin, command } = req.body;

  // Send MQTT message to control GPIO
  const topic = `/${node}/control`;
  const message = `${pin}:${command}`;
  client.publish(topic, message);

  res.send(`GPIO control request sent for ${node} - ${pin} (${command})`);
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
