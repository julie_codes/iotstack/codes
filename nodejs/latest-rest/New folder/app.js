const express = require('express');
const cors = require('cors');
const app = express();
//const server = require('http').Server(app);
const url = require('url');
var mqtt = require('mqtt');
const port = process.env.PORT || 6000;
const WebSocket = require('ws');
const http = require('http');


 // Create an HTTP server
const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('WebSocket Server\n');
});

// Combine the WebSocket and HTTP server 
// wss1 = camera feeds, wss2 = web clients
const wss1 = new WebSocket.Server({ noServer: true });
const wss2 = new WebSocket.Server({ server }); // host web on client-side

//var MongoClient = require('mongodb').MongoClient;

// maintain an array of camera feeds (register Name)
// Initialize an array to keep track of connected WebSocket video clients
// ### todo: record/maintain also streamStatus in here
const camClients = new Map(); //Map cliemt ID's to WebSocket connections
// maintain list of web client ID's
var clients=[];

//### todo maintain mapping client <-> (allowed) cams (store in Mongo)
//### todo create a ping/pong infra for scanning infrastructure

//incoming streams esp32cam websocket
wss1.on('connection', function connection(ws) {
  //console.log('Client connected');
  let clientId;
  ws.on('message', function incoming(message) {
    wss2.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
      }
    });
  });

  // Handle messages from clients
  ws.on('message', (message) => {
    // Parse the received message as JSON
    try {
      const parsedMessage = JSON.parse(message);
      //console.log(parsedMessage);
      //capture stream registration
      if (parsedMessage.type === 'register'){
          clientId = parsedMessage.id;
          const clientName = parsedMessage.name;
          // store the WS connection with camClient info
          // todo: add camera settings/IP etc to opts:{framerate, size, URL, Port, etc etc}
          camClients.set(clientId, {ws, name: clientName});
          console.log('Client ', clientName,' registered with ID ',clientId);
          //console.log(clients);
      }
      // Send any other JSON parsed message (camera ID and frame data) to all connected clients
      clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
          client.send(JSON.stringify(parsedMessage));
          //console.log(message);
        }
      });
    } catch  {
      // ### Todo: check contents sanity (before forwarding)
      // Todo: send only to subscribed_to_feeds users
      // Assume if not Json its binary or just text
      clients.forEach((client) => {
        // Check if the client is in an open state
        if (client.readyState === WebSocket.OPEN) {
          // Send the binary data to the client
          //client.send(message, { binary: true });
          console.log("bin sent");
        }
      });
    };
  });

  // Handle disconnection
  ws.on('close', () => {
    // remove client from list
    if (clientId){
        camClients.delete(clientId);
        console.log('camClient ${clientId} disconnected')
    }
  })
})

//webbrowser websocket
wss2.on('connection', function connection(ws) {
  let clientId;
  ws.on('message', function incoming(message) {
    //console.log('received wss2: %s', message);
    wss1.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message);
      }
    });
  });

  // Handle disconnection
  ws.on('close', () => {
    // remove client from list
    if (clientId){
        clients.delete(clientId);
        console.log('Webclient ${clientId} disconnected')
    }
  });
});

//server.on('upgrade', function upgrade(request, socket, head) {
server.on('upgrade', function upgrade(request, socket, head) {
  const pathname = url.parse(request.url).pathname;
  // pathname should be the name of a valid ESP32 (feed originator)
  console.log("Requested ",pathname);
  const camClient = camClients.has(entry => entry.name === pathname);
  //const camClient = (name) => camClients.find(entry => entry.name === `${pathname}`)?.name ?? ''
  if (camClient.name > "") {
    wss1.handleStream(request, socket, head, function done(ws) {
      // ###todo: activate full stream on ESP32
      // if(!camCLient.streaming){wss2.emit([{startVideo:1}])}; activate streaming
      
      // Send frames to designated Client
      ws.emit('connection', ws, request);
    });
  } else if (pathname === '/list') {
    wss2.emit('connection', ws, request);
    client.send(message);
  } else {
    socket.destroy();
  }
});


app.use(express.static('public'));

app.get('/', (req, res) => {
  	res.render('index', {});
});


server.listen(port, () => {
	  console.log(`App listening at http://localhost:${port}`)
})

