const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');
const sharp = require('sharp');

async function main() {
  // Load the Coco-SSD model
  const model = await cocoSsd.load('faster_rcnn_inception_v2');
  //const model = await cocoSsd.load();

  
  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });

  // Create an Express app
  const app = express();

  app.use(cors());

  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

  // Handle HTTP POST requests to /detect
  app.post('/detect', upload.single('image'), async (req, res) => {
    const imageBuffer = fs.readFileSync(req.file.path);
    
    // Resize the image to 800x800
    const image = await sharp(imageBuffer).resize(800, 800).toBuffer();
    console.log('Resized image shape:', await sharp(image).metadata());
  
    const decodedImage = tf.node.decodeImage(image, 3);
    console.log(decodedImage.shape);
    const predictions = await model.detect(decodedImage);
    console.log('Predictions:', predictions);
    
    const boxes = [];

    // Check that predictions is not empty and has a bbox property
    if (predictions && predictions.length > 0 && predictions[0].hasOwnProperty('bbox')) {

      // Loop through the predictions and push the bounding box information to the array
      for (const prediction of predictions) {
        const [ymin, xmin, ymax, xmax] = prediction.bbox;
        boxes.push({
          ymin: ymin * decodedImage.shape[0] / 800,
          xmin: xmin * decodedImage.shape[1] / 800,
          ymax: ymax * decodedImage.shape[0] / 800,
          xmax: xmax * decodedImage.shape[1] / 800,
          class: prediction.class
        });
      }
    } else {
      // Handle the case where predictions is empty or doesn't have a bbox property
      console.error('Invalid predictions');
    }

    // Construct the response object with the base64 encoded image and bounding boxes information
    const base64Image = Buffer.from(image).toString('base64');
    const response = {
      image: base64Image,
      boxes: boxes
    };
    res.send(response);
  });

  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
