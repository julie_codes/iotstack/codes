const { Number } = require('mongoose')
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SensorDataSchema = new Schema({
  deviceId: {
    type: String,
    required: true
  },
  msg: {
    type: Object,
    required: true,
    default: {}
  }
});

module.exports = mongoose.model('node01', SensorDataSchema);
