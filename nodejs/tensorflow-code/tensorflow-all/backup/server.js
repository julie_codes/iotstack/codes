const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');
const sharp = require('sharp');

async function main() {
  // Load the Coco-SSD model pest detection object detection
  const model = await cocoSsd.load('faster_rcnn_inception_v2');
  
  //Inception Model is for Classification
  const modelPath = 'https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1';
  //const model = await cocoSsd.load();
  let inceptModel;
  (async () => {
    inceptModel = await tf.loadGraphModel(modelPath, { fromTFHub: true });
    console.log('Inception model loaded');
  })();
  
  // Load the COCO-SSD model pest detection 
  let cocoModel;
  (async () => {
    cocoModel = await cocoSsd.load();
    console.log('COCO-SSD model loaded');
  })();

  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });

  // Create an Express app
  const app = express();

  app.use(cors());

  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

  // Handle HTTP POST requests to /detect
  app.post('/detect', upload.single('image'), async (req, res) => {
      console.log("detect api called");
    const imageBuffer = fs.readFileSync(req.file.path);
    
    // Resize the image to 800x800
    const image = await sharp(imageBuffer).resize(800, 800).toBuffer();
    console.log('Resized image shape:', await sharp(image).metadata());
  
    const decodedImage = tf.node.decodeImage(image, 3);
    console.log(decodedImage.shape);
    const predictions = await model.detect(decodedImage);
    console.log('Predictions:', predictions);
    
    const boxes = [];

    // Check that predictions is not empty and has a bbox property
    if (predictions && predictions.length > 0 && predictions[0].hasOwnProperty('bbox')) {

      // Loop through the predictions and push the bounding box information to the array
      for (const prediction of predictions) {
        const [ymin, xmin, ymax, xmax] = prediction.bbox;
        boxes.push({
          ymin: ymin * decodedImage.shape[0] / 800,
          xmin: xmin * decodedImage.shape[1] / 800,
          ymax: ymax * decodedImage.shape[0] / 800,
          xmax: xmax * decodedImage.shape[1] / 800,
          class: prediction.class
        });
      }
    } else {
      // Handle the case where predictions is empty or doesn't have a bbox property
      console.error('Invalid predictions');
    }

    // Construct the response object with the base64 encoded image and bounding boxes information
    const base64Image = Buffer.from(image).toString('base64');
    const response = {
      image: base64Image,
      boxes: boxes
    };
    res.send(response);
  });
  
  
  //ROUTE FOR COMPO CLASSIFIY 
  
  // Define a route to classify an image using Inception and detect pests using COCO-SSD
app.post('/classify', upload.single('image'), async (req, res) => {
    console.log("classify api inceptModel");
    const imageBuffer = fs.readFileSync(req.file.path);
  try {
    // Load the image from disk
    const imageBuffer = fs.readFileSync(req.file.path);
    const image = tf.node.decodeImage(imageBuffer);

    // Classify the image using Inception
    const inceptionOutput = inceptModel.predict(image);
    const inceptionPredictions = inceptionOutput.arraySync()[0];
    const topPrediction = inceptionPredictions.indexOf(Math.max(...inceptionPredictions));
    const vegetables = ['carrot', 'corn', 'lettuce', 'pepper', 'tomato'];
    const predictedVegetable = vegetables[topPrediction];

    // Detect pests using COCO-SSD
    const cocoPredictions = await cocoModel.detect(image);
    const pestsPresent = cocoPredictions.some((prediction) => prediction.class === 'person');

    // Construct the response object
    const response = {
      vegetable: predictedVegetable,
      pestsPresent: pestsPresent,
    };

    // Send the response
    res.json(response);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});
  
    // Handle HTTP POST requests to /classify
  app.post('/classify', upload.single('image'), async (req, res) => {
    const imageBuffer = fs.readFileSync(req.file.path);
    const predictedClassIndex = await classifyImage(imageBuffer);
	console.log('Classification Results:', predictedClassIndex);
    res.send({ predictedClassIndex });
  });

  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
