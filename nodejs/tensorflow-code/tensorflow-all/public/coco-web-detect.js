const tf = require('@tensorflow/tfjs-node');
const cocoSsd = require('@tensorflow-models/coco-ssd');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const fs = require('fs');

async function main() {
  // Load the Coco-SSD model
  const model = await cocoSsd.load();

  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });

  // Create an Express app
  const app = express();

  app.use(cors());

  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

  // Handle HTTP POST requests to /detect
  app.post('/detect', upload.single('image'), async (req, res) => {
    const imageBuffer = fs.readFileSync(req.file.path);
    const decodedImage = tf.node.decodeImage(imageBuffer, 3);
    const predictions = await model.detect(decodedImage);
    console.log('Predictions:', predictions);
    
    // Create an array to hold the bounding box information
    const boxes = [];

    // Loop through the predictions and push the bounding box information to the array
    for (const prediction of predictions) {
      const [ymin, xmin, ymax, xmax] = prediction.bbox;
      boxes.push({
        ymin: ymin,
        xmin: xmin,
        ymax: ymax,
        xmax: xmax,
        class: prediction.class
      });
    }

    // Convert the image to a base64 string
    const base64Image = Buffer.from(imageBuffer).toString('base64');

    // Construct the response object with the base64 encoded image and bounding boxes information
    const response = {
      image: base64Image,
      boxes: boxes
    };
    res.send(response);
  });

  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
