#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <Wire.h>
#include <BH1750.h>

#include<PubSubClient.h>
#include <ArduinoJson.h>
#include <EnvironmentCalculations.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <HTTPClient.h>
//#include <AsyncStaticWebHandler.h >

#include <OneWire.h>
#include <DallasTemperature.h>

#include <Ticker.h>
Ticker timer;
bool measurementInProgress = false;
//unsigned long previousMillis = 0;
//const unsigned long interval = 2000;  // Delay interval in milliseconds

int ledPin = 18;

const int SENSOR_PIN = 23; // Arduino pin connected to DS18B20 sensor's DQ pin

OneWire oneWire(SENSOR_PIN);         // setup a oneWire instance
DallasTemperature tempSensor(&oneWire); // pass oneWire to DallasTemperature library

float tempCelsius;    // temperature in Celsius
float tempFahrenheit; // temperature in Fahrenheit

const char* deviceId = "FarmBuddy_Sensor_Node02"; // set device ID here
const char* plantId = "Eggplant"; // set device ID here
const char * mqtt_broker = "192.168.100.5";
//const char * mqtt_broker = "broker.hivemq.com";
const char * mqtt_topic = "farmwise/node01/sensors/data"; // CHANGE SensorID here!
const char * mqtt_topic_subscribe = "farmwise/node01/sensors/data"; // CHANGE SensorID here!
//const char * mqtt_topic_subscribe = "farmwise/node01/gpio/status"; // CHANGE SensorID here!
const char * gpioTopic = "farmwise/node01/gpio/control"; // CHANGE SensorID here!
const char * mqtt_User = "";
const char * mqtt_Password = "";

//AP Variables
const char* ssid_ap = "FarmWise-Sensor";
const char* password_ap = "12345678";

WiFiClient espClient;
PubSubClient client(espClient);

const int numPins = 4;  // Total number of GPIO pins to control
int gpioPins[numPins] = {2, 4, 5, 18};  // Array of GPIO pins
bool gpioStates[numPins] = {false};  // Array of GPIO states

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Received message: ");
  String message = "";
  for (int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  Serial.println(message);

  // Parse the JSON message
  StaticJsonDocument<512> doc;
  DeserializationError error = deserializeJson(doc, message);
  if (error) {
    Serial.print("deserializeJson() failed: ");
    Serial.println(error.c_str());
    return;
  }

  // Check if the received message is for controlling a GPIO pin
  if (doc.containsKey("pin") && doc.containsKey("command")) {
    int pin = doc["pin"];
    String command = doc["command"];
    if (command == "on") {
      digitalWrite(pin, HIGH);
      Serial.print("GPIO Pin ");
      Serial.print(pin);
      Serial.println(" turned ON");
    } else if (command == "off") {
      digitalWrite(pin, LOW);
      Serial.print("GPIO Pin ");
      Serial.print(pin);
      Serial.println(" turned OFF");
    }
  }

}

char output[1023];

const char * ssid = "HUAWEI-2.4G-bF2k";//"HUAWEI-2.4G-bF2k";//"Team09"; //"Virus"; //"Team09"; //"Virus"; //"HUAWEI-3FDA";//"HUAWEI-3FDA"; // Enter SSID here
const char * password = "DuuBnx2S"; //"DuuBnx2S"; // "H@ckTe@m)("; //"RedEyeJedi44"; //"H@ckTe@m)(";  //"YB7DJY63ARE"; //"RedEyeJedi44"; //"YB7DJY63ARE"; // //Enter Password here

AsyncWebServer server(80);
// Set your Board ID (ESP32 Sender #1 = BOARD_ID 1, ESP32 Sender #2 = BOARD_ID 2, etc)
#define BOARD_ID 1
#define POWER_CTRL          4
#define BAT_ADC             33
#define SALT_PIN            34
#define SOIL_PIN            32
#define I2C_SDA             25
#define I2C_SCL             26
// Digital pin connected to the DHT sensor
#define DHTPIN 16
//#define DHTPIN 4

//float temp, hum;
float heatIndexF, f ;
float temp = 0;
float hum = 0;

//BH1750 lightMeter(0x23); //0x23

// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
DHT dht(DHTPIN, DHTTYPE);

//MAC Address of the receiver
uint8_t broadcastAddress[] = {0xB8, 0xD6, 0x1A, 0x5D, 0x2D, 0xDC};//{0x24, 0x6F, 0x28, 0x9E, 0x8D, 0x5C};
//B8:D6:1A:5D:2D:DC
//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
  int id;
  float temp;
  float hum;
  float soil;
  float salt;
  //float light;
  float batt;
  int readingId;
} struct_message;

//Create a struct_message called myData
struct_message myData;

unsigned long previousMillis = 0;   // Stores last time temperature was published
const long interval = 10000;        // Interval at which to publish sensor readings

unsigned int readingId = 0;

// Insert your SSID
constexpr char WIFI_SSID[] = "HUAWEI-2.4G-bF2k";

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
    for (uint8_t i = 0; i < n; i++) {
      if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
        return WiFi.channel(i);
      }
    }
  }
  return 0;
}

float readDHTTemperature() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  // Read temperature as Celsius (the default)
  float temp = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  //float t = dht.readTemperature(true);
  // Check if any reads failed and exit early (to try again).
  if (isnan(temp)) {
    Serial.println("Failed to read from DHT sensor!");
    return 0;
  }
  else {
    //Serial.println(temp);
    return temp;
  }
}

float readDHTHumidity() {
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float hum = dht.readHumidity();
  if (isnan(hum)) {
    Serial.println("Failed to read from DHT sensor!");
    return 0;
  }
  else {
    //Serial.println(hum);
    return hum;
  }
}

 //esp now callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void setup() {

  pinMode(ledPin, OUTPUT);

  //Gpio config
  for (int i = 0; i < numPins; i++) {
    pinMode(gpioPins[i], OUTPUT);
    digitalWrite(gpioPins[i], gpioStates[i] ? HIGH : LOW);
  }

  //Init Serial Monitor
  Serial.begin(115200);

  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  //config AP Mode
  Serial.println("\n[*] Creating AP");
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid_ap, password_ap);
  Serial.print("[+] AP Created with IP Gateway ");
  Serial.println(WiFi.softAPIP());

  //config Station Mode
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());
  client.setServer(mqtt_broker, 1883);
  client.setCallback(callback);

  if (initMqtt()) {
    client.publish("juliesundar/codettes/01", "Hello from ESP32");
  }

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html");
  });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(temp).c_str());
  });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(hum).c_str());
  });
  server.on("/heatIndex", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/plain", String(heatIndexF).c_str());
  });
  server.on("/js/highcharts.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/highcharts.js", "text/javascript");
  });
  server.on("/js/config.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/config.js", "text/javascript");
  });
  server.on("/js/jquery.min.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/jquery.min.js", "text/javascript");
  });
  server.on("/js/mqttws31.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/mqttws31.js", "text/javascript");
  });

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/home.html");
  });

  // Start server
  server.begin();

  Serial.println("HTTP server started");
  Wire.begin();

  dht.begin();
  // Start Temp probes
  tempSensor.begin();

  Serial.println("Connecting to ");
  Serial.println(ssid);
  pinMode(POWER_CTRL, OUTPUT);
  digitalWrite(POWER_CTRL, 1);

  //lux
  /* if (lightMeter.begin(BH1750::CONTINUOUS_HIGH_RES_MODE)) {
     Serial.println(F("BH1750 Advanced begin"));
    } else {
     //Serial.println(F("Error initialising BH1750"));
    }*/

  // Set device as a Wi-Fi Station and set channel
  WiFi.mode(WIFI_STA);
//
//
   // Check if there is an internet connection
  if (WiFi.status() == WL_CONNECTED) {
    // Internet connection available, skip ESP-NOW initialization
    Serial.println("Internet connection available. Skipping ESP-NOW initialization.");
  } else {
    // Internet connection not available, proceed with ESP-NOW initialization

    Serial.println("Connecting to " + String(ssid));
    pinMode(POWER_CTRL, OUTPUT);
    digitalWrite(POWER_CTRL, 1);

    // Init ESP-NOW
    if (esp_now_init() != ESP_OK) {
      Serial.println("Error initializing ESP-NOW");
      return;
    }

    // Once ESPNow is successfully Init, register for Send CB to
    // get the status of Trasnmitted packet
    esp_now_register_send_cb(OnDataSent);

    // Register peer
    esp_now_peer_info_t peerInfo;
    memcpy(peerInfo.peer_addr, broadcastAddress, 6);
    peerInfo.encrypt = false;

    // Add peer
    if (esp_now_add_peer(&peerInfo) != ESP_OK) {
      Serial.println("Failed to add peer");
      return;
    }

    Serial.println("ESP-NOW initialized");
  }

  int32_t channel = getWiFiChannel(WIFI_SSID);

  WiFi.printDiag(Serial); // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial); // Uncomment to verify channel change after

  //Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);

  //Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.encrypt = false;

  //Add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK) {
    Serial.println("Failed to add peer");
    return;
  }
}


uint16_t readSoil()
{
  uint16_t soil = analogRead(SOIL_PIN);
  return map(soil, 0, 4095, 100, 0);
  return soil;
}

uint32_t readSalt()
{
  uint8_t samples = 120;
  uint32_t humi = 0;
  uint16_t array[120];

  for (int i = 0; i < samples; i++) {
    array[i] = analogRead(SALT_PIN);
    delay(2);
  }
  std::sort(array, array + samples);
  for (int i = 0; i < samples; i++) {
    if (i == 0 || i == samples - 1)continue;
    humi += array[i];
  }
  humi /= samples - 2;
  return humi;
}

float readBattery()
{
  int vref = 1100;
  uint16_t volt = analogRead(BAT_ADC);
  float battery_voltage = ((float)volt / 4095.0) * 2.0 * 3.3 * (vref);
  return battery_voltage;
}


unsigned long lastMqttTime = millis();
const unsigned long Mqtt_INTERVAL_MS = 2000;

//unsigned long previousMillis2 = 0;
//const unsigned long interval2 = 1000;  // Adjust the interval as needed


void loop() {

  digitalWrite(POWER_CTRL, 1); // Turn power on dht
  //delay(1000);

  client.loop();

  // Send regular mqtt data
  if ((millis() - lastMqttTime) > Mqtt_INTERVAL_MS) {
    if (client.connected())
    {
      //GetWeatherData();
      handle_MqttData();
      sendWeatherData();
      lastMqttTime = millis();
    } else
    {
      initMqtt();
    }

  }

  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // Save the last time a new reading was published
    previousMillis = currentMillis;
    //Set values to send
    myData.id = BOARD_ID;
    myData.temp = readDHTTemperature();
    myData.hum = readDHTHumidity();
    myData.soil = readSoil();
    myData.salt = readSalt();
    //myData.light = lightMeter.readLightLevel();
    myData.batt = readBattery();
    myData.readingId = readingId++;

    //Send message via ESP-NOW
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &myData, sizeof(myData));
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");

    }
  }
  digitalWrite(POWER_CTRL, 1); // Turn power on dht
}

bool initMqtt()
{
  if (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP32Client", mqtt_User, mqtt_Password )) {

      Serial.println("connected");

      return true;

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
      return false;
    }
  }
}

/*bool initBMP()
  {
  dht.begin();
  }*/

uint16_t soil;
uint32_t salt;
//float light;
float batt;

void sendWeatherData() {

  temp = dht.readTemperature();
  hum = dht.readHumidity();
  uint16_t soil = readSoil();
  uint32_t salt = readSalt();
  // float light = lightMeter.readLightLevel();
  float batt = readBattery();
  float hum = dht.readHumidity();
  float temp = dht.readTemperature();


  tempSensor.requestTemperatures();             // send the command to get temperatures
  tempCelsius = tempSensor.getTempCByIndex(0);  // read temperature in Celsius
  tempFahrenheit = tempCelsius * 9 / 5 + 32; // convert Celsius to Fahrenheit
  //Serial.print("tempCelsius");
  //Serial.print(tempCelsius);
  delay(2000);

  HTTPClient http;
  String url = "http://api.openweathermap.org/data/2.5/weather?q=Paramaribo&APPID=6185be2ed84ee299aaea2515c1a63684";
  String requestBody = "{\"wifiAccessPoints\": [";
  int numNetworks = WiFi.scanNetworks();
  for (int i = 0; i < numNetworks; i++) {
    if (i > 0) {
      requestBody += ",";
    }
    String bssid = WiFi.BSSIDstr(i);
    int rssi = WiFi.RSSI(i);
    requestBody += "{\"macAddress\":\"" + bssid + "\",\"signalStrength\":" + String(rssi) + "}";
  }
  requestBody += "]}";
  http.begin(url);
  http.addHeader("Content-Type", "application/json");
  int httpResponseCode = http.POST(requestBody);
  if (httpResponseCode == HTTP_CODE_OK) {
    String responseBody = http.getString();

    DynamicJsonDocument jsonDoc(2048);
    deserializeJson(jsonDoc, responseBody);
    jsonDoc.clear();
    
    float lat = jsonDoc["coord"]["lat"];
    float lon = jsonDoc["coord"]["lon"];
    
    // Create a new JSON document and add the data
    StaticJsonDocument<2048> doc;
    doc.clear();
    
    doc["deviceId"] = deviceId;
    doc["plantId"] = plantId;
    doc["temp"] = temp;
    doc["humid"] = hum;
    doc["heatIndex"] = heatIndexF;
    doc["soil"] = soil;
    doc["salt"] = salt;
    // doc["light"] = light;
    doc["batt"] = batt;
    doc["tempCelsius"] = tempCelsius; // assign temperature value to JSON key //NOTE FAILS WHEN TRYING TO SEND TO MQTT
   
    
    JsonObject location = doc.createNestedObject("location");
    location["latitude"] = lat;
    location["longitude"] = lon;
       
    char out[2048];
    serializeJson(doc, out);
    Serial.println(out);
  
        
    // Publish the data to the MQTT broker
    client.publish((char*)mqtt_topic, out) ? Serial.println(" -> delivered") : Serial.println(" -> failed");
    client.subscribe("farmwise/node01/gpio/control");
    client.subscribe("farmwise/node01/sensors/data");
  } else {
    Serial.println("Failed to send request");
  }
  http.end();
}


unsigned long weatherPreviousMillis = 0;
const unsigned long weatherInterval = 5000;  // Weather data sending interval in milliseconds

void handle_MqttData() {
  unsigned long weatherCurrentMillis = millis();

  // Check if the weather data sending interval has passed
  if (weatherCurrentMillis - weatherPreviousMillis >= weatherInterval) {
    weatherPreviousMillis = weatherCurrentMillis;
    sendWeatherData();
  }
}
