const express = require('express');
const tf = require('@tensorflow/tfjs-node');
const fs = require('fs');
const app = express();

// Load the detection class indeces
const labels = require("./model/model5custom/inverted_class_indices.json"); //model2working

	
// Route to handle image prediction
app.get('/predict', async (req, res) => {
  try {
	// debug if labels loaded
	//console.log('Labels in Model: ');
	//console.log(labels);

  async function predictDisease(){
	// Load the model
	let modelPath = 'model/model5custom/model.json'; //note model2 working
	let model = tf.loadLayersModel(`file://${modelPath}`);
	console.log(model);
	console.log("model loaded");
	// Load image from file system
    const imagePath = 'image/image.JPG';
    let img = fs.readFileSync(imagePath); //document.getElementById('plant-photo')
    // Decode and preprocess image
    let imageTensor = tf.node.decodeImage(img);
    let offset = tf.scalar(255)
    let tensorImg = imageTensor.resizeNearestNeighbor([224, 224]).toFloat().expandDims();
	//let preprocessedImage = tf.cast(tf.image.resizeBilinear(imageTensor, [224, 224]), 'float32').div(255);

    let tensorImg_scaled = tensorImg.div(offset);
	//let prediction = await model.then((model) => model.predict(preprocessedImage.reshape([1, 224, 224, 3])));
    let prediction = await model.then((model) => model.predict(tensorImg_scaled).data());

    let predicted_class = tf.argMax(prediction)
    let class_idxs = Array.from(predicted_class.dataSync());
    let diseases = [];

    for (let class_idx of class_idxs) {
      let predictedDisease = labels[class_idx];
	  console.log(predictedDisease);
      let [name, disease] = predictedDisease.split('___');
      name = name.replaceAll('_', ' ');
      disease = disease.replaceAll('_', ' ');
      diseases.push({
        name,
        disease,
        //image: `https://coverimages.blob.core.windows.net/disease-cover-images/${predictedDisease}.JPG`,
        cureURL: encodeURI(`https://www.google.com/search?q=How to cure ${disease} in ${name}`)
      }) 
    }
	console.log(diseases);
    return diseases;
  }
  // ## nd insert
/*
    // Make prediction using model
    const prediction = await model.then((model) => model.predict(preprocessedImage.reshape([1, 224, 224, 3])));
	//const prediction = await model.then((model) => model.predict(preprocessedImage.reshape([1, 256, 256, 3])));
    const predictedClassIndex = prediction.argMax(axis=1).dataSync()[0];
	const classNames = ['Potato___Early_blight', 'Potato___healthy', 'Potato___Late_blight'];
	const predictedClassName = classNames[predictedClassIndex];
	console.log('prediction:' ,predictedClassName);
    // Get the confidence score
	const confidence = tf.max(prediction).dataSync()[0] * 100;

	// Log the prediction result
	console.log(`Predicted class: ${predictedClassName}, Confidence: ${confidence.toFixed(2)}%`);

    // Send predicted class index as response to client
*/
    res.send(predictDisease()); //(`Predicted class index: ${predictedClassIndex}`);
  } catch (error) {
    console.error(error);
    res.status(500).send('An error occurred while predicting image.');
  }
});

// Start the server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
