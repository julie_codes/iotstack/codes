const tf = require('@tensorflow/tfjs-node');
const mobilenet = require('@tensorflow-models/mobilenet');
const knnClassifier = require('@tensorflow-models/knn-classifier');
const jpeg = require('jpeg-js');
const fs = require('fs');
const axios = require('axios');
const express = require('express');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
//const IMAGENET_CLASSES = require('./imagenet_classes'); //module
const { IMAGENET_CLASSES } = require('./imagenet_classes'); // objects



async function main() {
  // Load the pre-trained model from the TensorFlow.js model zoo
   //const model = await mobilenet.load();
  //const modelPath = 'model/model.json';
  
  //const model = await tf.loadLayersModel(`file://${modelPath}`);
  //const modelPath = 'model/model.json';
  //const model = await tf.loadGraphModel(`file://${modelPath}`);
 
 const modelPath = 'https://tfhub.dev/google/tfjs-model/imagenet/inception_v3/classification/3/default/1';
 const model = await tf.loadGraphModel(modelPath, { fromTFHub: true });

  console.log(model);

  // Load the KNN classifier
  const classifier = knnClassifier.create();

  // Preprocess the image
  const preprocessImage = (imageBuffer) => {
    const tfImage = tf.node.decodeImage(imageBuffer, 3); // adjst based on model
    //const tfResizedImage = tf.image.resizeBilinear(tfImage, [128, 128]); // adjust based on model
	const tfResizedImage = tf.image.resizeBilinear(tfImage, [299, 299]);
    const tfFloatImage = tf.cast(tfResizedImage, 'float32');
    const tfScaledImage = tfFloatImage.div(tf.scalar(255));
    const tfBatchedImage = tfScaledImage.expandDims(0);
    return tfBatchedImage;
  };


   
// Classify the image using the model
const classifyImage = async (imageBuffer) => {
  const tfBatchedImage = preprocessImage(imageBuffer);
  const prediction = await model.predict(tfBatchedImage);
  const values = await prediction.data();
  const index = values.indexOf(Math.max(...values));
  const className = IMAGENET_CLASSES[index];
  return { className, index };
};


  
  // Configure multer to receive uploaded files
  const upload = multer({
    storage: multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'uploads/')
      },
      filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
      }
    })
  });

  // Create an Express app
  const app = express();

  app.use(cors());
  // Serve the HTML file
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

  // Handle HTTP POST requests to /classify
  app.post('/classify', upload.single('image'), async (req, res) => {
    const imageBuffer = fs.readFileSync(req.file.path);
    const predictedClassIndex = await classifyImage(imageBuffer);
	console.log('Classification Results:', predictedClassIndex);
    res.send({ predictedClassIndex });
  });

  // Start the server
  app.listen(3000, () => {
    console.log('Server started on port 3000');
  });
}

main();
``
