
#include <WiFi.h>
#include<PubSubClient.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include <HTTPClient.h>

bool measurementInProgress = false;

const char* deviceId = "FarmBuddy_Sensor_Node02"; // set device ID here
const char* plantId = "Eggplant"; // set device ID here
const char * mqtt_broker = "192.168.100.5";
const char * mqtt_topic = "farmwise/node01/sensors/data"; // CHANGE SensorID here!
const char * mqtt_topic_subscribe = "farmwise/node01/sensors/data"; // CHANGE SensorID here!
//const char * mqtt_topic_subscribe = "farmwise/node01/gpio/status"; // CHANGE SensorID here!
const char * gpioTopic = "farmwise/node01/gpio/control"; // CHANGE SensorID here!
const char * mqtt_User = "";
const char * mqtt_Password = "";

//AP Variables
const char* ssid_ap = "FarmWise-Sensor";
const char* password_ap = "12345678";

WiFiClient espClient;
PubSubClient client(espClient);

const int numPins = 4;  // Total number of GPIO pins to control
int gpioPins[numPins] = {2, 4, 5, 18};  // Array of GPIO pins
bool gpioStates[numPins] = {false};  // Array of GPIO states

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Received message: ");
  String message = "";
  for (int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  Serial.println(message);

  // Parse the JSON message
  StaticJsonDocument<512> doc;
  DeserializationError error = deserializeJson(doc, message);
  if (error) {
    Serial.print("deserializeJson() failed: ");
    Serial.println(error.c_str());
    return;
  }

  // Check if the received message is for controlling a GPIO pin
  if (doc.containsKey("pin") && doc.containsKey("command")) {
    int pin = doc["pin"];
    String command = doc["command"];
    if (command == "on") {
      digitalWrite(pin, HIGH);
      gpioStates[pin] = true;
      publishGPIOStatus(pin, true); // Publish the status when turned ON
      Serial.print("GPIO Pin ");
      Serial.print(pin);
      Serial.println(" turned ON");
    } else if (command == "off") {
      digitalWrite(pin, LOW);
      gpioStates[pin] = false;
      publishGPIOStatus(pin, false); // Publish the status when turned OFF
      Serial.print("GPIO Pin ");
      Serial.print(pin);
      Serial.println(" turned OFF");
    }
  }

}

char output[1023];

const char * ssid = "HUAWEI-2.4G-bF2k";//"HUAWEI-2.4G-bF2k";//"Team09"; //"Virus"; //"Team09"; //"Virus"; //"HUAWEI-3FDA";//"HUAWEI-3FDA"; // Enter SSID here
const char * password = "DuuBnx2S"; //"DuuBnx2S"; // "H@ckTe@m)("; //"RedEyeJedi44"; //"H@ckTe@m)(";  //"YB7DJY63ARE"; //"RedEyeJedi44"; //"YB7DJY63ARE"; // //Enter Password here

AsyncWebServer server(80);
// Set your Board ID (ESP32 Sender #1 = BOARD_ID 1, ESP32 Sender #2 = BOARD_ID 2, etc)
#define BOARD_ID 1

unsigned long previousMillis = 0;   // Stores last time temperature was published
const long interval = 10000;        // Interval at which to publish sensor readings

unsigned int readingId = 0;

// Insert your SSID
constexpr char WIFI_SSID[] = "HUAWEI-2.4G-bF2k";

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
    for (uint8_t i = 0; i < n; i++) {
      if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
        return WiFi.channel(i);
      }
    }
  }
  return 0;
}



 
void setup() {


  //Gpio config
  for (int i = 0; i < numPins; i++) {
    pinMode(gpioPins[i], OUTPUT);
    digitalWrite(gpioPins[i], gpioStates[i] ? HIGH : LOW);
  }

  //Init Serial Monitor
  Serial.begin(115200);

  if (!SPIFFS.begin()) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  //config AP Mode
  Serial.println("\n[*] Creating AP");
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid_ap, password_ap);
  Serial.print("[+] AP Created with IP Gateway ");
  Serial.println(WiFi.softAPIP());

  //config Station Mode
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected..!");
  Serial.print("Got IP: ");
  Serial.println(WiFi.localIP());
  client.setServer(mqtt_broker, 1883);
  client.setCallback(callback);

  if (initMqtt()) {
    client.publish("juliesundar/codettes/01", "Hello from ESP32");
  }

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html");
  });

  server.on("/js/highcharts.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/highcharts.js", "text/javascript");
  });
  server.on("/js/config.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/config.js", "text/javascript");
  });
  server.on("/js/jquery.min.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/jquery.min.js", "text/javascript");
  });
  server.on("/js/mqttws31.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/js/mqttws31.js", "text/javascript");
  });

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/home.html");
  });

  // Start server
  server.begin();

  Serial.println("Connecting to ");
  Serial.println(ssid);

  // Set device as a Wi-Fi Station and set channel
  WiFi.mode(WIFI_STA);

}

unsigned long lastMqttTime = millis();
const unsigned long Mqtt_INTERVAL_MS = 2000;


void loop() {

  client.loop();

  // Send regular mqtt data
  if ((millis() - lastMqttTime) > Mqtt_INTERVAL_MS) {
    if (client.connected())
    {
      //GetWeatherData();
     handle_MqttData();
      sendWeatherData();
      lastMqttTime = millis();
    } else
    {
      initMqtt();
    }

  }

}

bool initMqtt()
{
  if (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP32Client", mqtt_User, mqtt_Password )) {

      Serial.println("connected");

      return true;

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
      return false;
    }
  }
}


void sendWeatherData() {

    client.subscribe("farmwise/node01/gpio/control");
    client.subscribe("farmwise/node01/sensors/data");
 } 

unsigned long weatherPreviousMillis = 0;
const unsigned long weatherInterval = 5000;  // Weather data sending interval in milliseconds

void handle_MqttData() {
  unsigned long weatherCurrentMillis = millis();

  // Check if the weather data sending interval has passed
  if (weatherCurrentMillis - weatherPreviousMillis >= weatherInterval) {
    weatherPreviousMillis = weatherCurrentMillis;
    sendWeatherData();
  }
}

void publishGPIOStatus(int pin, bool state) {
  String status = state ? "on" : "off";
  String message = "{\"pin\":" + String(pin) + ",\"status\":\"" + status + "\"}";
  client.publish("farmwise/node01/gpio/status", message.c_str());
}
