from flask import Flask, request, jsonify
from flask_cors import CORS  # Import the CORS extension
from keras.models import load_model
from PIL import Image
import numpy as np
import io

app = Flask(__name__)
CORS(app)  # Enable CORS for all routes

# Load the trained models
model1_path = 'model1.h5'
model2_path = 'model2.h5'
model1 = load_model(model1_path)
model2 = load_model(model2_path)

# Set the input size for your models
img_size = (224, 224)

# Define class names for each model
class_names_model1 = ['Apple___Apple_scab', 'Apple___Black_rot', 'Apple___Cedar_apple_rust', 'Apple___healthy']
class_names_model2 = ['Blueberry___healthy', 'Cherry_(including_sour)___healthy', 'Cherry_(including_sour)___Powdery_mildew', 'Corn_(maize)___Cercospora_leaf_spot Gray_leaf_spot']

# Define a function to process the image and make predictions
def process_image(img):
    # Resize the image to match the input size of the models
    img = img.resize(img_size)
    # Convert image to array
    img_array = np.array(img)
    # Expand dimensions to match batch size
    img_array = np.expand_dims(img_array, axis=0)
    # Normalize pixel values
    img_array = img_array.astype('float32') / 255.0
    # Make predictions using both models
    prediction1 = model1.predict(img_array)
    prediction2 = model2.predict(img_array)
    return prediction1, prediction2, img_array

# Define an endpoint to receive image uploads
@app.route('/predict', methods=['POST'])
def predict_image():
    # Check if the request contains an image
    if 'image' not in request.files:
        return jsonify({'error': 'No image provided'}), 400
    # Get the uploaded image
    file = request.files['image']
    # Open the image using PIL
    img = Image.open(file)
    # Process the image
    prediction1, prediction2, processed_img = process_image(img)
    # Calculate the weighted probabilities
    weighted_prediction1 = prediction1 * 0.5  # Assuming equal weights for both models
    weighted_prediction2 = prediction2 * 0.5
    # Get the predicted class with the highest probability among both models
    if np.max(weighted_prediction1) > np.max(weighted_prediction2):
        predicted_class = class_names_model1[np.argmax(weighted_prediction1)]
        prediction = weighted_prediction1.tolist()
    else:
        predicted_class = class_names_model2[np.argmax(weighted_prediction2)]
        prediction = weighted_prediction2.tolist()
    # Convert processed image array back to image
    processed_img = Image.fromarray(processed_img[0].astype('uint8'))
    # Save processed image to in-memory file
    img_io = io.BytesIO()
    processed_img.save(img_io, format='JPEG')
    img_io.seek(0)
    # Return JSON response without including the processed image
    response = {
        'prediction': prediction,
        'predicted_class': predicted_class
    }
    return jsonify(response), 200

if __name__ == '__main__':
    app.run(debug=True)
