const draggables = document.querySelectorAll(".task");
const lanes = document.querySelectorAll(".lanes");
const widgetSection = document.getElementById("todo-lane");


draggables.forEach((task) => {
  task.addEventListener("dragstart", () => {
    task.classList.add("is-dragging");
  });

  task.addEventListener("dragend", () => {
    task.classList.remove("is-dragging");
  });
});

lanes.forEach((lane) => {
  lane.addEventListener("dragover", (e) => {
    e.preventDefault();
    
    const curTask = document.querySelector(".is-dragging");
    const closestTask = getClosestTask(curTask, e.clientY);

    if (closestTask) {
      const rect = closestTask.getBoundingClientRect();
      const posY = e.clientY - rect.top;
      const threshold = rect.height / 2;

      if (posY > threshold) {
        closestTask.parentNode.insertBefore(curTask, closestTask.nextSibling);
      } else {
        closestTask.parentNode.insertBefore(curTask, closestTask);
      }
    }
  });

// Check if the current lane is not the "swim-lane"
// Check if the current lane is not the "swim-lane"
if (!lane.classList.contains("swim-lane")) {
  lane.addEventListener("drop", (e) => {
    e.preventDefault();
    const curTask = document.querySelector(".is-dragging");
    lane.appendChild(curTask);

    const taskText = curTask.textContent.trim().replace(/\s+/g, " "); // Trim and remove extra spaces

    let newTask = widgetSection.querySelector(`.task[data-text="${taskText}"]`);

// ...

if (!newTask) {
  newTask = document.createElement("div");
  newTask.classList.add("task");
  newTask.draggable = true;
  newTask.setAttribute("data-text", taskText);

  const taskTextElement = document.createElement("span");
  taskTextElement.textContent = taskText;

  const settingsIcon = document.createElement("span");
  settingsIcon.classList.add("settings-icon");
  const iconElement = document.createElement("i");
  iconElement.classList.add("fa", "fa-cog");
  settingsIcon.appendChild(iconElement);

  newTask.appendChild(taskTextElement);
  newTask.appendChild(settingsIcon);

  widgetSection.appendChild(newTask);

  newTask.addEventListener("dragstart", () => {
    newTask.classList.add("is-dragging");
  });

  newTask.addEventListener("dragend", () => {
    newTask.classList.remove("is-dragging");
  });
}

  });
}


});

const getClosestTask = (curTask, mouseY) => {
  const tasks = [...curTask.parentElement.querySelectorAll(".task")];
  return tasks.reduce(
    (closest, task) => {
      const rect = task.getBoundingClientRect();
      const offset = mouseY - rect.top;

      if (offset < 0 && offset > closest.offset) {
        return { offset, element: task };
      } else {
        return closest;
      }
    },
    { offset: Number.NEGATIVE_INFINITY }
  ).element;
};
