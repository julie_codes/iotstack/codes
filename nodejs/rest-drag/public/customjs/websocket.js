
  
      var chart;
      var mqtt;
      var topic = "juliesundar/codettes/01"; // Specify the topic here
      var responseObject = {}; // define responseObject variable
      var reconnectTimeout = 2000;

 
    function MQTTconnect() {
	if (typeof path == "undefined") {
		path = '/mqtt';
	}
	mqtt = new Paho.MQTT.Client(
			host,
			port,
			path,
			"web_" + parseInt(Math.random() * 100, 10)
	);
        var options = {
            timeout: 3,
            useSSL: useTLS,
            cleanSession: cleansession,
            onSuccess: onConnect,
            onFailure: function (message) {
                $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
                setTimeout(MQTTconnect, reconnectTimeout);
            }
        };

        mqtt.onConnectionLost = onConnectionLost;
        mqtt.onMessageArrived = onMessageArrived;

        if (username != null) {
            options.userName = username;
            options.password = password;
        }
        console.log("Host="+ host + ", port=" + port + ", path=" + path + " TLS = " + useTLS + " username=" + username + " password=" + password);
        mqtt.connect(options);
    }

function onConnect() {
    $('#status').val('Connected to ' + host + ':' + port + path);
    // Connection succeeded; subscribe to our topic
    mqtt.subscribe(topic, {qos: 0});
    $('#topic2').val(topic);

    // Set up chart
    chart = Highcharts.chart('chart-container', {
      chart: {
        type: 'line',
        turboThreshold: 10000, // set to 0 to disable thresholding and display all points
         scrollablePlotArea: {
          minWidth: 600,
          scrollPositionX: 1
        }
      },
      title: {
        text: 'Sensor Data'
      },
      xAxis: {
        type: 'datetime',
        title: {
          text: 'Timestamp'
        }
      },
      yAxis: [{
        title: {
          text: 'Temperature'
        }
      }, {
        title: {
          text: 'Humidity'
        },
        opposite: true // display on opposite side of chart
      }, {
        title: {
          text: 'Soil Moisture'
        },
        opposite: true // display on opposite side of chart
      }, {
        title: {
          text: 'Salt'
        },
        opposite: true // display on opposite side of chart
      }, {
        title: {
          text: 'Light'
        },
        opposite: true // display on opposite side of chart
      }, {
        title: {
          text: 'Battery'
        },
        opposite: true // display on opposite side of chart
      }],
      series: [{
        name: 'Temperature',
        data: [],
        yAxis: 0 // use the first yAxis (index 0) for this series
      }, {
        name: 'Humidity',
        data: [],
        yAxis: 1 // use the second yAxis (index 1) for this series
      }, {
        name: 'Soil Moisture',
        data: [],
        yAxis: 2 // use the third yAxis (index 2) for this series
      }, {
        name: 'Salt',
        data: [],
        yAxis: 3 // use the fourth yAxis (index 3) for this series
      }, {
        name: 'Light',
        data: [],
        yAxis: 4 // use the fifth yAxis (index 4) for this series
      }, {
        name: 'Battery',
        data: [],
        yAxis: 5 // use the sixth yAxis (index 5) for this series
      }]
    });
}

  function onConnectionLost(responseObject) {
  setTimeout(MQTTconnect, reconnectTimeout);
  $('#status').val("Connection lost: " + responseObject.errorMessage + ". Reconnecting");
}

function onMessageArrived(message) {
  var topic = message.destinationName;
  var payload = message.payloadString;
  $('#ws').prepend('<li>' + topic + ' = ' + payload + '</li>');
  console.log("Message received: " + payload);

  // Parse message data
  var data = JSON.parse(payload);
  var timestamp = new Date().getTime();
  var temp = parseFloat(data.temp);
  var humid = parseFloat(data.humid);
  var soil = parseFloat(data.soil);
  var salt = parseFloat(data.salt);
  var light = parseFloat(data.light);
  var batt = parseFloat(data.batt);

  // Widgets
  // Get all the widget containers
  const widgetContainers = document.querySelectorAll('.widget-container');

  // Loop through each widget container and update the widget data
  widgetContainers.forEach(widgetElement => {
    widgetElement.querySelector('.temp-data').textContent = data.temp;
    widgetElement.querySelector('.humid-data').textContent = data.humid;
    widgetElement.querySelector('.soil-data').textContent = data.soil;
    widgetElement.querySelector('.salt-data').textContent = data.salt;
    widgetElement.querySelector('.light-data').textContent = data.light;
    widgetElement.querySelector('.batt-data').textContent = data.batt;
  });

  // Update the chart data
  chart.series[0].addPoint([timestamp, temp]);
  chart.series[1].addPoint([timestamp, humid]);
  chart.series[2].addPoint([timestamp, soil]);
  chart.series[3].addPoint([timestamp, salt]);
  chart.series[4].addPoint([timestamp, light]);
  chart.series[5].addPoint([timestamp, batt]);

  // Redraw the chart
  chart.redraw();

  // Update the chart based on the toggle switch
  if ($("#toggle-switch").is(":checked")) {
    // Display historical data
    var startDate = new Date($("#start-date").val());
    var endDate = new Date($("#end-date").val());
    chart.xAxis[0].setExtremes(startDate.getTime(), endDate.getTime());
  } else {
    // Display live data
    chart.xAxis[0].setExtremes(); // Reset the x-axis extremes to display all data
  }
}

$(document).ready(function() {
  MQTTconnect();
});

function fetchData(start, end) {
  console.log('fetchData called with start:', start, 'end:', end);
  const url = `/data/chart-data?start=${start.toISOString()}&end=${end.toISOString()}`;  
  console.log('fetchData url:', url);
  fetch(url)
    .then(response => response.json())
    .then(data => {
      console.log('fetchData data:', data);
      const chartData = data.map((obj) => ({
        x: new Date(obj.msg.timestamp).getTime(),
        y: [obj.msg.temp, obj.msg.humid, obj.msg.soil, obj.msg.salt, obj.msg.light, obj.msg.battery],
      }));
      console.log('fetchData chartData:', chartData);
      chart.series[0].setData(chartData.map(obj => [obj.x, obj.y[0]]));
      chart.series[1].setData(chartData.map(obj => [obj.x, obj.y[1]]));
      chart.series[2].setData(chartData.map(obj => [obj.x, obj.y[2]]));
      chart.series[3].setData(chartData.map(obj => [obj.x, obj.y[3]]));
      chart.series[4].setData(chartData.map(obj => [obj.x, obj.y[4]]));
      chart.series[5].setData(chartData.map(obj => [obj.x, obj.y[5]]));
    })
    .catch(error => console.error(error));
}

function handleSubmit(event) {
    event.preventDefault();
    const start = new Date(document.getElementById('start-date').value);
    const end = new Date(document.getElementById('end-date').value);
    console.log(start, end);
    fetchData(start, end);
}

    const myform = document.getElementById('date-form');
    myform.addEventListener('submit', handleSubmit);
