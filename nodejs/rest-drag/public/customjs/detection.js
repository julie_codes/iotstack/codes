 
    const pestform = document.querySelector('#pestForm');
    const responseDiv = document.querySelector('#response');
    const imageContainer = document.querySelector('#image-container');

    pestform.addEventListener('submit', async (event) => {
      event.preventDefault();
      responseDiv.innerHTML = 'Detecting pests...';
      imageContainer.innerHTML = '';

      const formData = new FormData(event.target);
      const response = await fetch('/detect', {
        method: 'POST',
        body: formData,
      });
      const data = await response.json();
      console.log(data);
      responseDiv.innerHTML = '';

     // if (data.pestsPresent) {
        const img = document.createElement('img');
        img.src = 'data:image/jpeg;base64,' + data.img;
        img.onload = () => {
          const canvas = document.createElement('canvas');
          /*canvas.width = img.width;
          canvas.height = img.height;*/
          canvas.width = 150;
          canvas.height = 150;
          const ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0,150,150);
          
          imageContainer.appendChild(canvas);
        };

        const p = document.createElement('p');
        p.textContent = ` Predicted diseases: ${data.diseases[0].disease}`;
        responseDiv.appendChild(p);
    });
