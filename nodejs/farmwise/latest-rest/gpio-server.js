const express = require('express');
const app = express();
const mqtt = require('mqtt');

// MQTT client setup
const client = mqtt.connect([{ host: 'localhost', port: '1883' }]);
const statusTopic = 'farmwise/node01/gpio/status';

// Handle MQTT messages
client.on('message', function (topic, message) {
  if (topic === statusTopic) {
    console.log('Received status update:', message.toString());
  }
});

// Subscribe to the status topic
client.on('connect', function () {
  client.subscribe(statusTopic, function (err) {
    if (err) {
      console.log('Failed to subscribe to status topic:', err);
    } else {
      console.log('Subscribed to status topic');
    }
  });
});

app.get('/control-gpio', function (req, res) {
  // Extract GPIO pin and command from the query parameters
  const { pin, command } = req.query;

  // Publish GPIO control command to the MQTT topic
  client.publish('farmwise/node01/gpio/control', JSON.stringify({ pin, command }), function (err) {
    if (err) {
      console.error('Failed to publish GPIO control command:', err);
      res.status(500).send('Failed to control GPIO');
    } else {
      console.log('GPIO control command published:', pin, command);
      res.status(200).send('GPIO control command sent');
    }
  });
});

// Start the server
app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
